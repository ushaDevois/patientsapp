import Expo, { Permissions, Notifications } from 'expo';

export default async () => {
  console.log("test0..")
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;
console.log("test1..", finalStatus);
  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
    console.log("test2..", finalStatus);
  }
console.log("test3..")
  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
  	console.log("test4..")
    return;
  }
console.log("test5..")
  // Get the token that uniquely identifies this device
  try {
   let token = await Expo.Notifications.getExpoPushTokenAsync();
   console.log("token.......",token);
  } catch (err) {
  console.log("err", err);
 }
}