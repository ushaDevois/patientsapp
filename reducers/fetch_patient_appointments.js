import {
  FETCH_PATIENT_APPOINTMENT
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PATIENT_APPOINTMENT:
    	return action.payload 
    default:
      return state;
  }
}