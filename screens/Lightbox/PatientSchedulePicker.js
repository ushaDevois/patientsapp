import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
  } from 'react-native';

import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../../constants/styles'
import Lightbox from './BaseLightbox'

import AvailableIcon from '../../assets/images/available.png'

import ToggleButton from '../../components/ToggleButton'
import HorizontalCalendar from '../../components/HorizontalCalendar'
import Button from '../../components/Button'

import * as actions from '../../actions';
import { connect } from 'react-redux';
import updatePatientAppointment from '../../actions/updatePatientAppointment';
import { AsyncStorage } from 'react-native';


class PatientSchedulePicker extends React.Component {
  constructor(props) {
    super(props);
   
    this.state = {
      visible: true,
      date: "",
      time: '',
      patient_id_load: props.patient_id
    }
//    console.log("props....", props);
    this._payScreen = this._payScreen.bind(this);
  }

  _onBookNow = () => {

  }

  componentDidMount(){
   // let phone_number = awaitAsyncStorage.getItem('phone_number');
 //   this.props.loggedMember();
  }

  closeModal = () => {
    this.setState({visible: false})
  }

  _payScreen(){
  
    updatePatientAppointment.PatientAppointment(this.state.time,
      this.state.date, this.props.data.doctor_id, this.state.patient_id_load, this.props.data.type);
    Actions.patient_confirm_pay();
  }

  cancel = () => {
  //  this.props.fetch_doctor_details(null, false);
    Actions.doctor_details();
  }

  render() {

    let userCallType = this.props.data.type == "audio" ? "Book a audio appointment" : "Book a video appointment";
    let callTypePay = this.props.data.type == "audio" ? "$20" : "$30";
    return (
      <Lightbox vertical={0.92} align="bottom" visible={this.state.visible}>
        <View style={styles.container}>

          <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: colors.blue, padding: 8}}>
            <View style={{flex: 0.75}}>
              <Image source={AvailableIcon} />
            </View>
            <View style={{flex: 4, marginRight: 8}}>
              <Text style={{color: colors.white, fontSize: fontSizes['sm']}}>Dr. Mamie Wagner is available for immediete consultation.</Text>
            </View>
            <View style={{flex: 2}}>
              <Button size="sm" background={colors.dark.text} onPress={this._onBookNow}>Book now</Button>
            </View>
          </View>

          <Text style={styles.title}>Select your convenient date and time </Text>

          <View>
            <HorizontalCalendar style={styles.calendar} prev={1} next={14} startDate={new Date} onSelect={(date) => {this.setState({date: date})}} />
            <Text style={[styles.smallText, {textAlign: 'center', paddingVertical: 8}]}>Scroll to move through the dates</Text>
          </View>
          <View style={{marginHorizontal: 32, marginVertical: 16}}>
            <Text style={[styles.smallText ,{marginVertical: 16}]}>SELECT A TIME SLOT</Text>
            <View style={styles.timingsContainer}>
              {['06:30PM', '07:00PM', '07:30PM', '08:00PM', '08:30PM', '09:30PM'].map((time, i) => {
                return (<ToggleButton style={{marginRight: 8, marginTop: 8}} key={i} onChange={() => {this.setState({time: time})}}>{time}</ToggleButton>)
              })}
            </View>
          </View>
          <View style={styles.buttonsContainer}>
            <View style={[styles.row, {justifyContent: 'space-between'}]}>
              <Button size="md" style={[styles.smallButton, {marginRight: 8}]} onPress={this._payScreen.bind(this)}>
                {userCallType} {"\n"}
                <Text style={{fontSize: 13, color: colors.lightWhite}}>pay {callTypePay}</Text>
              </Button>
              
            </View>
            <View style={[styles.row]}>
              <Button size="lg" background="transparent" style={styles.largeButton} onPress={this.cancel}>
                 Cancel {"\n"}
                
              </Button>
            </View>
          </View>
        </View>
      </Lightbox>
    )
  }
}

function mapStateToProps(state){
  if(state.member_logged != null){
   let patient_id = Object.values(state.member_logged)[0].user_id;
   console.log("patient_id////////", patient_id, state.fetch_patient_schedule_picker);
      return { patient_id: patient_id, data: state.fetch_patient_schedule_picker }
  }
  return {test: "test"}
}

export default connect(mapStateToProps, actions)(PatientSchedulePicker);


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    color: colors.dark.text,
    fontSize: fontSizes['lg'],
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 24
  },
  calendar: {
    backgroundColor: colors.background
  },
  smallText: {
    color: colors.text,
    fontSize: 13
  },
  timingsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  buttonsContainer: {
    marginHorizontal: 32,
    marginVertical: 16
  },
  smallButton: {
    flex: 1,
    paddingVertical: 8
  },
  largeButton: {
    flex: 1,
    paddingVertical: 8
  },
  row: {
    flexDirection: 'row',
    marginVertical: 8
  },
})
