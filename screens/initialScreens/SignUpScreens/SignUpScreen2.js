import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  Keyboard,

} from 'react-native';

import { LinearGradient } from 'expo'

import { colors, fontSizes, gradients } from '../../../constants/styles'
import LeftArrow from '../../../assets/images/left_arrow.png'

import Dropdown from '../../../components/Dropdown'
import TextField from '../../../components/TextField'
import Button from '../../../components/Button'
import { DocumentPicker, ImagePicker } from 'expo';

import RangeSlider from '../../../components/RangeSlider'
import ProgressWizard from '../../../components/ProgressWizard'
import ProfilePicture from '../../../assets/images/profile_picture.png'
import Link from '../../../components/Link'
import * as actions from '../../../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import DateTimePicker from 'react-native-modal-datetime-picker';

class SignUpScreen2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dob: '',
      bloodGroup: '',
      res_address: '',
      isDateTimePickerVisible: false
    }
   
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this._submitNext = this._submitNext.bind(this);
  }

  static navigationOptions = {
    
  };

  _submitNext(){
//    console.log("signup3");
   
    let signupData = {};
    signupData.dob = this.state.dob;
    signupData.bloodGroup = this.state.bloodGroup;
    signupData.res_address = this.state.res_address;
    this.props.store_input3(signupData);
    Actions.signup4();
  }

  handleChangeValue(field_name, textInput){
   // console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "res_address"){
      this.setState({res_address: textValue});
    }     
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
 
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
 
  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);
    this._hideDateTimePicker();
    this.setState({dob: date});
  };

  _handleDateOfBirth = () => {
   // Keyboard.dismiss(); //TODO; this is a temporary hack
    Actions.date_picker();
  }

  _onSetProfilePicture = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    console.log(result, result.uri);
    if(result.type == "success"){
      this.props.uploadAsFile(result.uri);
    }
  }

  render() {
    var bloodGroups = [
      {title: 'A+ve'},
      {title: 'A-ve'},
      {title: 'B+ve'},
      {title: 'B-ve'},
      {title: 'AB+ve'},
      {title: 'AB-ve'},
      {title: 'O+ve'},
      {title: 'O-ve'}
    ]

    const overlay = this.state.submitted ? (
      <View style={styles.overlay}></View>
    ) : null

    return (
      <View style={styles['container']}>
        <LinearGradient
          style={styles['headerCard']}
          colors={gradients.grayWhite}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={styles['subtitle']}>Fill up the following fields to finish signup</Text>
           <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={2}/>
          </View>
        </LinearGradient>
        <ScrollView style={styles['form']}>
          <TouchableOpacity style={styles.back} onPress={() => {}}>
            <Image source={LeftArrow} style={{marginRight: 8}}/>
            <Text style={{color: colors.blue, fontSize: fontSizes['sm']}}>BACK</Text>
          </TouchableOpacity>
          
          <View style={styles['formItem']}>
            <Text style={styles['label']}>BLOOD GROUP</Text>
            <Dropdown title={'bloodgroup'} list={bloodGroups} onSelect={(obj,i) => this.setState({bloodGroup: obj.title})}></Dropdown>
          </View>

          <View style={styles['formItem']}>
            <TextField
              label="RESIDENTIAL ADDRESS"
              style={{flex: 1, marginLeft: 8}}
              inputStyle={{}}
              prop_name="res_address"
              value={this.state.res_address}
              onChange={this.handleChangeValue}
              placeholder="Enter your address"/>

          </View>
          <View style={styles['formItem']}>
            <Link onPress={this._showDateTimePicker}>Select Date Of Birth</Link>
            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />

          </View>
          <View style={[styles['formItem'], {flexDirection: 'row', alignItems: 'center'}]}>
            <Image source={ProfilePicture} style={{marginRight: 8}}/>
            <Link onPress={this._onSetProfilePicture}>Set a profile picture</Link>
          </View>
        </ScrollView>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._submitNext} >Create account</Button>
        </LinearGradient>
        {overlay}
      </View>
    )
  }
}

export default connect(null, actions)(SignUpScreen2);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  title: {
    fontSize: 20,
    color: colors.dark.text,
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4
  },
  headerCard: {
    paddingHorizontal: 24,
    paddingVertical: 22
  },
  back: {
    flexDirection: 'row',
    marginBottom: 8
  },
  form: {
    paddingHorizontal: 24
  },
  bottomCard: {
    padding: 24,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  label: {
    paddingVertical: 4,
    color: colors.text,
    fontSize: 13
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 10,
    backgroundColor: colors.fadedWhiteText
  },
  input: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.dark.background,
    padding: 8,
    height: 50
  }
})