import {
  FETCH_FAVOURITE_DOCTORS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const updateFavouriteDoctor = (data) => async dispatch => {
		console.log("updateFavouriteDoctor", data);
	  const Data = {
	    patient_id: data.patient_id,
	    doctor_id: data.doctor_id,
	    favourite: data.favourite	    
	  }

//      console.log('patientAppointment....', Data);

      let ref = firebase.database().ref(`patients/favourite`);
	
	
	ref.orderByChild("patient_id").equalTo(data.patient_id).on("value", function(snapshot) {
      // console.log('MEMBER_LOGGED=--------',snapshot.key);
       
       if(snapshot.val() != null || snapshot.val() != undefined){
	       let arrycurrentPatients = Object.values(snapshot.val());   
	       arrycurrentPatients.map((list,i) => {
	       		if(data.doctor_id == list.doctor_id){
	       			ref.orderByChild("doctor_id").equalTo(data.doctor_id).on("child_added", function(snapshotvalue) {
		//		       console.log('key=--------',snapshotvalue.key);
				       let currentKey = snapshotvalue.key;
				       firebase.database().ref(`patients/favourite/${currentKey}`).update({
				        "favourite": data.favourite
				      });
				   });
	       		}
	       		else{

	       			firebase.database().ref(`patients/favourite`)
	          			.push(Data);
	       		}
	       })
	    }
	    else{
	    	firebase.database().ref(`patients/favourite`)
	          			.push(Data);
	    }
       
   });
}

export const FetchFavouriteDoctors = () => async dispatch => {
	let phoneNumber = await AsyncStorage.getItem('phone_number');
	let ref4 = firebase.database().ref(`ratings`);
    let ref3 = firebase.database().ref(`patients/favourite`);
	let ref1 = firebase.database().ref(`doctors/Signup`);
	let ref2 = firebase.database().ref(`patients/Signup`);
    //console.log("I am in FetchFavouriteDoctors");

	await ref2.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot){
	//	console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let user_id = Object.values(snapshot.val())[0].user_id;
	//			console.log("user_id...", user_id);
			console.log("Testing  user_id....", user_id);
			    
			
			ref3.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
			
			 if(snapshot.val() != null || snapshot.val() != undefined){
				let arrycurrentPatients = Object.values(snapshot.val());  
				let newResultArray = [];
				let mergedObj = {};	
	//			console.log("arrycurrentPatients....", arrycurrentPatients);
				arrycurrentPatients.map((list,i) => {
	//				console.log("list..///", list);
					if(list.favourite == true){
						let obj = {};        
				        ref1.orderByChild("user_id").equalTo(list.doctor_id).on("value", function(snap) {
	//			          console.log("snap,,,,,,", snap.val());
				          if(snap.val() != null || snap.val() != undefined){  
				            let presentfavourite = Object.values(snap.val());   
	//			            console.log("presentfavourite...", presentfavourite);
	
							ref4.orderByChild("user_id").equalTo(list.doctor_id).on("value", function(ratingval) {				           
							if(ratingval.val() != null || ratingval.val() != undefined){  
				            let presentfavouriterating = Object.values(ratingval.val())[0];   

					            mergedObj = presentfavourite[0]; 
					            obj.user_id = mergedObj.user_id,
					            obj.name = mergedObj.first_name,
					          	obj.avatar = mergedObj.avatar,
					          	obj.rating = presentfavouriterating.overAllRatings,
					          	obj.language = ["English"],
					          	obj.location = mergedObj.res_address,
					          	obj.repliesIn = presentfavouriterating.responseTime,
					          	obj.experience = mergedObj.experience + "+",
					          	obj.specialization = mergedObj.specialization,
					          	obj.isOnline = true		//need to be fetched
					            newResultArray.push(obj);
	//				            console.log("newResultArray....", newResultArray);
					            dispatch({ type: FETCH_FAVOURITE_DOCTORS, payload: newResultArray });
					        }
					        });
					     }
					  });
						
					}
				}) 
			  }
			})
		}
	})

}

