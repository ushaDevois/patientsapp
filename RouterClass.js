import Expo, { Notifications } from 'expo';
import registerForNotifications from './services/pushNotifications';

import React from 'react';
import { StyleSheet, Text, View, Alert, Dimensions } from 'react-native';

import { Drawer, ActionConst, Scene, Reducer, Router, Switch, TabBar, Modal, Schema, Actions, Overlay,
  Tabs, Stack, Lightbox } from 'react-native-router-flux';

import TopNavBar from './components/TopNavBar';
import DrawerContent from './components/DrawerContent'
import MenuIcon from './assets/images/menu_hamburger.png';

import WelcomeScreen from './screens/initialScreens/welcomeScreen';
import TermsAndConditions from './screens/TermsAndConditions';
import PhoneVerification from './screens/initialScreens/PhoneVerification/PhoneVerification';
import Countries from './screens/Countries';
import SignUpScreen1 from './screens/initialScreens/SignUpScreens/SignUpScreen1';
import SignUpScreen2 from './screens/initialScreens/SignUpScreens/SignUpScreen2'; 
import DatePicker from './screens/Lightbox/DatePickerLightbox';

import SignUpScreen4 from './screens/initialScreens/SignUpScreens/SignUpScreen4';
import UploadCertificates from './screens/initialScreens/SignUpScreens/UploadCertificates';
import SignUpScreen6Upload from './screens/initialScreens/SignUpScreens/SignUpScreen6Upload';

import DoctorSpecialities from './screens/DoctorsInteractions/DoctorSpecialities';
import DoctorsList from './screens/DoctorsInteractions/DoctorsList';
import DoctorDetail from './screens/DoctorsInteractions/DoctorDetail';
import ChatBot from './screens/DoctorsInteractions/ChatBot/ChatBot';
import FilterDoctors from './screens/DoctorsInteractions/FilterDoctors';
import LexBot from './screens/DoctorsInteractions/LexBot';


import IncomingCall from './screens/IncomingCall';
import VideoCall from './screens/VideoCall';
import PatientSchedulePicker from './screens/Lightbox/PatientSchedulePicker';
import PatientConfirmPay from './screens/Lightbox/PatientConfirmPay';
import Cardreader from './screens/CardReader';

import Bookings from './screens/Bookings';
import PatientCancelAppointment from './screens/Lightbox/PatientCancelAppointment';

import Chats from './screens/Chats/Chats';
import Chat from './screens/Chats/Chat';
import UserProfile from './screens/Lightbox/UserProfile';
import ChatFileUpload from './screens/Lightbox/ChatFileUpload';
import ChatWithUpload from './screens/Chats/ChatWithUpload';

import PatientProfile from './screens/PatientProfile';
import UploadedFiles from './screens/SelectUploadedFiles';
import PhotoCrop from './screens/PhotoCrop';
import RemoveCard from './screens/Lightbox/RemoveCard';
import CallFeedback from './screens/CallFeedback';
import QuoteLoader from './screens/Lightbox/QuoteLoader';

import FavoriteDoctors from './screens/FavoriteDoctors';
import RemoveFavoriteDoctor from './screens/Lightbox/RemoveFavoriteDoctor';

import NotificationsList from './screens/NotificationsList';

import FeedbackCard from './components/FeedbackCard';
import FeedbackCardEnjoy from './components/FeedbackCardEnjoy';
import FeedbackCardRating from './components/FeedbackCardRating';


export default class RouterClass extends React.Component {

  componentDidMount() {
    registerForNotifications();
    Notifications.addListener((notification) => {
      const { data: { text } } = notification;
      console.log("PUSH NOTIFICATIONS.....", text);
      if(notification.origin === 'received' && text){
        Alert.alert(
            'New Push Notification',
            test,
            [{ text: 'OK.' }]
          );
      }

    });
  }


  render() {    
      const {width, height} = Dimensions.get('window');  
    return (      
          <Router>
            <Lightbox key="lightbox" hideNavBar>
              <Stack key="root" hideNavBar>
                <Stack key="navbar" navBar={TopNavBar}>
                  <Scene key="welcome" component={WelcomeScreen} hideNavBar={true} />
                  <Scene key="terms" component={TermsAndConditions} title="Terms of use"  />
                  <Scene key="phoneverification" component={PhoneVerification} title="Verify" hideNavBar={true}/>
                  <Scene key="countries" component={Countries} title="Countries"/> 
                  <Scene key="signup1" component={SignUpScreen1} title="Signup" />
                  <Scene key="signup2" component={SignUpScreen2} title="Signup" />

                  <Scene key="date_picker" component={DatePicker} />

                  <Scene key="signup4" component={SignUpScreen4} title="Signup" />
                  <Scene key="uploadcertificates" component={UploadCertificates} title="Upload Certificates" />
                  <Scene key="sigupupload6" component={SignUpScreen6Upload} title="Sigup Upload" />

                  <Drawer
                    hideNavBar
                    key="drawer"
                    contentComponent={DrawerContent}
                    drawerImage={MenuIcon}
                    drawerWidth={0.7 * width}>
                    <Scene key="doctor_specialities" component={DoctorSpecialities} title="Choose Speciality" />
                    <Scene key="doctors_list" component={DoctorsList} title="Doctors List" />
                    <Scene key="doctor_details" component={DoctorDetail} title="Doctor Details" />
                    <Scene key="lexbot" component={LexBot} title="Clinical Assistant" />
                    <Scene key="chatbot" component={ChatBot} title="Chat Bot" />
                    <Scene key="patient_confirm_pay" component={PatientConfirmPay} />
                    <Scene key="filter_doctors" component={FilterDoctors} title="Filter Doctors" />
                    
                    <Scene key="incoming_call" component={IncomingCall} title="Incoming Call" />
                    
                    <Scene key="patient_schedule_picker" component={PatientSchedulePicker} hideNavBar={true} />
                    <Scene key="card_reader" component={Cardreader} />

                    <Scene key="patient_bookings" component={Bookings} title="Bookings" />
                    <Scene key="patient_cancel_appointment" component={PatientCancelAppointment} />
                    <Scene key="video_call" component={VideoCall} hideNavBar={true} />


                    <Scene key="chats" component={Chats} title="Chats" />   
                    <Scene key="userprofile" component={UserProfile} />
                    <Scene key="chat" component={Chat} title="Chat" />
                    <Scene key="chatupload" component={ChatFileUpload} />
                    <Scene key="chatwithfileupload" component={ChatWithUpload} title="Chat" />

                    <Scene key="patient_profile" component={PatientProfile} title="Profile" />
                    <Scene key="upload_files" component={UploadedFiles} title="Select the Files" />
                    <Scene key="photo_edit" component={PhotoCrop} />
                    <Scene key="remove_card" component={RemoveCard} />
                    <Scene key="feedback_form" component={CallFeedback} title="Your Feedback" />
                    <Scene key="quote_loader" component={QuoteLoader} />
                    <Scene key="randfb1" component={FeedbackCard} title="FeedbackCard" />
                    <Scene key="randfb2" component={FeedbackCardEnjoy} title="FeedbackCardEnjoy" />
                    <Scene key="randfb3" component={FeedbackCardRating} title="FeedbackCardRating" />

                    <Scene key="notifications" component={NotificationsList} title="Notifications" />

                    <Scene key="favorite_doctor" component={FavoriteDoctors} title="Favorite Doctors" />
                    <Scene key="remove_favorite_doctor" component={RemoveFavoriteDoctor} />
                  </Drawer>
                </Stack>
              </Stack>              
            </Lightbox>              
          </Router>        
    );
  }
}