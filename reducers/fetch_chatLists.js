import {
  GET_CHAT_LISTS,
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case GET_CHAT_LISTS:
      return action.payload
    default:
      return state;
  }
}