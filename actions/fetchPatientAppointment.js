import {
  FETCH_PATIENT_APPOINTMENT,
  SUCCESS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';
import addToNotifications from './notification_actions';

export const addToken = () => dispatch => {

	let  previousToken = AsyncStorage.getItem('pushtoken');
	console.log("previous Token= ", previousToken);

/*
	 const data1 = {
		    user_id: "SDIMllRU4naWvwf",
		    expoToken: "ExponentPushToken[ZiIzn8JRgYPjF5MXtiOIb_]"
	    };
	 const data2 = {
		    user_id: "pMQoB0QF4sN8tRM",
		    expoToken: "ExponentPushToken[ZiIzn8JRgYPjF5MXtiOIb_]"
	    };
	   
	    firebase.database().ref(`users/Tokens`)
	      .push(data1)
	      .then(() => {
	        dispatch({ type: SUCCESS });        
	      })
	      .catch(() => {
	        console.log("data1 Pushed");
	      });

	      firebase.database().ref(`users/Tokens`)
	      .push(data2)
	      .then(() => {
	        dispatch({ type: SUCCESS });        
	      })
	      .catch(() => {
	        console.log("data1 Pushed");
	      });		   
		
*/


	//if(previousToken) {
	//	return;
	//}
  
 /*	
    let ref = firebase.database().ref(`patients/Signup`);
	let phoneNumber = data;
	ref.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot) {
      // console.log('key=--------',snapshot.key);
       let user_id = Object.values(snapshot.val())[0].user_id;
        const data1 = {
		    user_id: user_id,
		    expoToken: previousToken
	    };
	   
	    firebase.database().ref(`users/Tokens`)
	      .push(data1)
	      .then(() => {
	        dispatch({ type: SUCCESS });        
	      })
	      .catch(() => {
	        console.log("data1 Pushed");
	      });		   
		
 	});*/
}

export const fetchPatientAppointment = (patient_id) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	let ref2 = firebase.database().ref(`doctors/Signup`);
	let patientId = patient_id;

	await ref.orderByChild("patient_id").equalTo(patientId).on("value", function(snapshot) {
        console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let arrPatientAppointments = Object.values(snapshot.val());
			let arrayRequestedAppointments = [];
			arrPatientAppointments.map((data,i) => {
				obj = {};
				if(data.status == "requested"){
					ref2.orderByChild("doctor_id").equalTo(data.doctor_id).on("value", function(doctorval){
						if(doctorval.val() != null || doctorval.val() != undefined){
							let arrPatientAppointments = Object.values(doctorval.val())[0];
							obj.first_name = arrPatientAppointments.first_name;
							obj.last_name = arrPatientAppointments.last_name;
							obj.avatar =arrPatientAppointments.avatar;
							obj.specialization = arrPatientAppointments.specialization;
							
							obj.type = data.type;
							obj.schedule_date = data.schedule_date;
							obj.schedule_time = data.schedule_time;
							arrayRequestedAppointments.push(obj);
							dispatch({ type: FETCH_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
						}
					});  
				}
			});
		}    	       
    });  
}


export const updateCompletedAppointment = (data, time) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	console.log("updateCompletedAppointment...//", data, time);
	let patientId = data.patient_id;
	let doctorId = data.doctor_id;
	let id = data.id;
	let endCallTime = time.toString();

	await ref.orderByChild("id").equalTo(id).on("child_added", function(doctorval){
		if(doctorval.val() != null || doctorval.val() != undefined){
			let currentKey = doctorval.key;
			console.log("currentKey...", currentKey);
	        firebase.database().ref(`users/PatientAppointments/${currentKey}`).update({
		        status: "Completed",
		        endCallTime: endCallTime
	        });
		}

		message = 'your Appointment has been Completed';
        addToNotifications.frameNotification(doctorId, patientId, message);
	});	
}


export const cancelPatientAppoitment = (data) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	console.log("cancelPatientAppoitment...//", data);
	let patientId = data.patient_id;
	let doctorId = data.doctor_id;
	let id = data.id;

	await ref.orderByChild("id").equalTo(id).on("child_added", function(doctorval){
		if(doctorval.val() != null || doctorval.val() != undefined){
			let currentKey = doctorval.key;
			console.log("currentKey...", currentKey);
	        firebase.database().ref(`users/PatientAppointments/${currentKey}`).update({
		        status: "Cancelled"
	        });
		}

		message = 'your Appointment has been Cancelled';
		addToNotifications.frameNotification(doctorId, patientId, message);
	});	
      

/*
	await ref.orderByChild("patient_id").equalTo(patientId).on("value", function(snapshot) {
        console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let arrPatientAppointments = Object.values(snapshot.val());
			let arrayRequestedAppointments = [];
			arrPatientAppointments.map((data,i) => {
				ref.orderByChild("id").equalTo(id).on("child_added", function(doctorval){
					if(doctorval.val() != null || doctorval.val() != undefined){
						let currentKey = doctorval.key;
						console.log("currentKey...", currentKey);
				        firebase.database().ref(`users/PatientAppointments/${currentKey}`).update({
					        status: "Cancelled"
				        });
					}
				});				
			});
		}    	       
    });*/  
}