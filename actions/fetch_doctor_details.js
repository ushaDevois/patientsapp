import {
  FETCH_DOCTOR_DETAILS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const fetch_doctor_details = (item, status) => async dispatch => {
	console.log("FETCH_DOCTOR_DETAILS", item);
	let data = {};
	
	if(status == true){
		data.avatar = item.avatar;
		data.experience = item.experience.toString();
		data.isOnline = item.isOnline.toString();
		data.language = item.language.toString();
		data.location = item.location;
		data.name = item.name;
		data.rating = item.rating.toString();
		data.repliesIn = item.repliesIn.toString();
		data.specialization = item.specialization;
		data.user_id = item.user_id;
		AsyncStorage.setItem('doctorDetail_avatar', item.avatar);
		AsyncStorage.setItem('doctorDetail_experience', item.experience.toString());
		AsyncStorage.setItem('doctorDetail_isOnline', item.isOnline.toString());
		AsyncStorage.setItem('doctorDetail_language', item.language.toString());
		AsyncStorage.setItem('doctorDetail_location', item.location);
		AsyncStorage.setItem('doctorDetail_name', item.name);
		AsyncStorage.setItem('doctorDetail_repliesIn', item.repliesIn.toString());
		AsyncStorage.setItem('doctorDetail_specialization', item.specialization);
		AsyncStorage.setItem('doctorDetail_user_id', item.user_id);
		AsyncStorage.setItem('doctorDetail_rating', item.rating.toString());
		data.status = "true";
		console.log(data);
  		dispatch({ type: FETCH_DOCTOR_DETAILS, payload: data });
	}else {
		data.avatar = AsyncStorage.getItem('doctorDetail_avatar');
		data.experience = AsyncStorage.getItem('doctorDetail_experience');
		data.isOnline = (AsyncStorage.getItem('doctorDetail_isOnline') == 'true') ? true : false;
		data.language = Array.from(AsyncStorage.getItem('doctorDetail_language'));
		data.location = AsyncStorage.getItem('doctorDetail_location');
		data.name = AsyncStorage.getItem('doctorDetail_name');
		data.repliesIn = parseInt(AsyncStorage.getItem('doctorDetail_repliesIn'), 10);
		data.specialization = AsyncStorage.getItem('doctorDetail_specialization');
		data.user_id = AsyncStorage.getItem('doctorDetail_user_id');
		data.rating = parseFloat(AsyncStorage.getItem('doctorDetail_rating'));
		
		data.status = "false";
		console.log(data);
		dispatch({ type: FETCH_DOCTOR_DETAILS, payload: data });
	}
}