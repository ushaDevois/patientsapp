import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import { CustomText } from '../CustomText'
import { colors, fontSizes } from '../../constants/styles'

const CustomButton = (props) => (
  <TouchableOpacity style={styles.button} onPress={props.onPress}>
    <CustomText>{props.title.toUpperCase()}</CustomText>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.white,
    paddingHorizontal: 20,
    paddingVertical: 14,
    borderLeftColor: colors.separator,
    borderLeftWidth: 2,
    flex: 1,
    flexDirection: "row",
    justifyContent: "center"
  },

  text: {
    fontSize: fontSizes.text
  }
})

export default CustomButton
