import {
  FETCH_INPUT,
  FETCH_INPUT3,
  FETCH_PROFILE_DATA,
  MEMBER_LOGGEDIN,
  MEMBER_LOGGED,
  FETCH_DOCTORS_LIST,
  FETCH_FAVOURITE_DOCTORS,
  FETCH_PATIENT_APPOINTMENT,
  FETCH_PAST_PATIENT_APPOINTMENT,
  FETCH_UPCOMMING_PATIENT_APPOINTMENT,
  FETCH_NOTIFICATIONS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

import moment from 'moment';

export const store_input1 = (data) => async dispatch => {
	console.log("llll", data);
	await AsyncStorage.setItem('phone_number', data);
}

export const store_input2 = (data) => async dispatch => {
	console.log("mmmm....", data);
	await AsyncStorage.setItem('first_name', data.first_name);	
	await AsyncStorage.setItem('last_name',  data.last_name);
	await AsyncStorage.setItem('email', data.email);
	await AsyncStorage.setItem('gender', data.gender);
}

export const store_input3 = (data) => async dispatch => {
	console.log("nnnn....", data);
	await AsyncStorage.setItem('dob', data.dob);	
	await AsyncStorage.setItem('bloodGroup',  data.bloodGroup);
	await AsyncStorage.setItem('res_address', data.res_address);	
}


export const randomString = (len, charSet) => {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
  }

export const store_signup = () => async dispatch => {

	var randomTask_id = randomString(15, '');
  
	let user_id =  await AsyncStorage.setItem('user_id', randomTask_id);
	let phone_number = await AsyncStorage.getItem('phone_number');
	let first_name = await AsyncStorage.getItem('first_name');
	let last_name = await AsyncStorage.getItem('last_name');
	let email = await AsyncStorage.getItem('email');
	let gender = await AsyncStorage.getItem('gender');
	let bloodGroup = await AsyncStorage.getItem('bloodGroup');
	let res_address = await AsyncStorage.getItem('res_address');
	
	
	let Data = {
		user_id: randomTask_id,
		first_name: first_name,
		last_name: last_name,
		email: email,
		gender: gender,
		bloodGroup: bloodGroup,
		res_address: res_address,
		phone_number: phone_number
		
	};
	//console.log("DATA............", Data);
	//check including the urls
	firebase.database().ref(`patients/Signup`).push(Data);
}

export const Fetch_ProfileData = (userId) => async dispatch => {
	var ref = firebase.database().ref(`patients/Signup`);
	let user_id = userId;
    ref.orderByChild("user_id").equalTo(user_id).on("value", function(snapshot) {
      dispatch({ type: FETCH_PROFILE_DATA, payload: snapshot.val() });         
    });      
}

export const update_profile = (data) => async dispatch => {
	let ref = firebase.database().ref(`patients/Signup`);
	let user_id = "6AmeAVBhXMmpv99";
    ref.orderByChild("user_id").equalTo(user_id).on("child_added", function(snapshot) {
      // console.log('key=--------',snapshot.key);
       let currentKey = snapshot.key;
       firebase.database().ref(`doctors/Signup/${currentKey}`).update({
        "first_name": data.first_name,
        "last_name": data.last_name,
        "email": data.email,
        "res_address": data.res_address
      });
   });
}

export const loggedinMember = (data) => async dispatch => {
	let ref = firebase.database().ref(`patients/Signup`);
	//console.log('......', data);
	let phoneNumber = data;
	ref.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot) {
      // console.log('key=--------',snapshot.key);
       AsyncStorage.setItem('user_id', Object.values(snapshot.val())[0].user_id);
       dispatch({ type: MEMBER_LOGGEDIN, payload: snapshot.val() });
    });

    //	Fetch list of FavouriteDoctors();
    let ref4 = firebase.database().ref(`ratings`);
    let ref3 = firebase.database().ref(`patients/favourite`);
	let ref1 = firebase.database().ref(`doctors/Signup`);
	let ref2 = firebase.database().ref(`patients/Signup`);
    //console.log("I am in FetchFavouriteDoctors");

	await ref2.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot){
	//	console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let user_id = Object.values(snapshot.val())[0].user_id;
	//			console.log("user_id...", user_id);
console.log("Testing  user_id....", user_id);
			ref.orderByChild("user_id").equalTo(user_id).on("value", function(snapshot) {
				console.log("Fetch_ProfileData...", snapshot.val());
			  dispatch({ type: FETCH_PROFILE_DATA, payload: Object.values(snapshot.val()) });         
			});    
			
			ref3.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
			
			 if(snapshot.val() != null || snapshot.val() != undefined){
				let arrycurrentPatients = Object.values(snapshot.val());  
				let newResultArray = [];
				let mergedObj = {};	
	//			console.log("arrycurrentPatients....", arrycurrentPatients);
				arrycurrentPatients.map((list,i) => {
	//				console.log("list..///", list);
					if(list.favourite == true){
						let obj = {};        
				        ref1.orderByChild("user_id").equalTo(list.doctor_id).on("value", function(snap) {
	//			          console.log("snap,,,,,,", snap.val());
				          if(snap.val() != null || snap.val() != undefined){  
				            let presentfavourite = Object.values(snap.val());   
	//			            console.log("presentfavourite...", presentfavourite);
	
							ref4.orderByChild("user_id").equalTo(list.doctor_id).on("value", function(ratingval) {				           
							if(ratingval.val() != null || ratingval.val() != undefined){  
				            let presentfavouriterating = Object.values(ratingval.val())[0];   

					            mergedObj = presentfavourite[0]; 
					            obj.user_id = mergedObj.user_id,
					            obj.name = mergedObj.first_name,
					          	obj.avatar = mergedObj.avatar,
					          	obj.rating = presentfavouriterating.overAllRatings,
					          	obj.language = ["English"],
					          	obj.location = mergedObj.res_address,
					          	obj.repliesIn = presentfavouriterating.responseTime,
					          	obj.experience = mergedObj.experience + "+",
					          	obj.specialization = mergedObj.specialization,
					          	obj.isOnline = true		//need to be fetched
					            newResultArray.push(obj);
	//				            console.log("newResultArray....", newResultArray);
					            dispatch({ type: FETCH_FAVOURITE_DOCTORS, payload: newResultArray });
					        }
					        });
					     }
					  });
						
					}
				}) 
			  }
			})


			//fetchRequestedPatientAppointment(user_id);


			var ref5 = firebase.database().ref(`users/PatientAppointments`);
			let ref6 = firebase.database().ref(`doctors/Signup`);
			//let patientId = patient_id;
			//console.log("fetchPatientAppointment......");

			ref5.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
		       //console.log("snapshot...", snapshot.val());
				if(snapshot.val() != null || snapshot.val() != undefined){
					let arrPatientAppointments = Object.values(snapshot.val());
					//console.log(arrPatientAppointments);
					let arrayRequestedAppointments = [];
					arrPatientAppointments.map((data,i) => {
						//console.log("data...", data, i);
						
						if(data.status == "Pending"){
							ref6.orderByChild("user_id").equalTo(data.doctor_id).on("value", function(doctorval){
								//console.log("doctorval", doctorval);
								if(doctorval.val() != null || doctorval.val() != undefined){
									let obj = {};
									let relevantDoctor = Object.values(doctorval.val())[0];
									//console.log("relevantDoctor", relevantDoctor);
									obj.first_name = relevantDoctor.first_name;
									obj.last_name = relevantDoctor.last_name;
									obj.avatar =relevantDoctor.avatar;
									obj.specialization = relevantDoctor.specialization;
									obj.id = data.id;
									obj.patient_id = user_id;
									obj.doctor_id = data.doctor_id;
									obj.type = data.type;
									obj.schedule_date = data.schedule_date;
									obj.schedule_time = data.schedule_time;
									obj.status = data.status;
									arrayRequestedAppointments.push(obj);
									//console.log("arrayRequestedAppointments...", arrayRequestedAppointments);
									
									dispatch({ type: FETCH_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
								}
							});  
						}
					});
				}    	       
		    });  



		    ////fetchPastPatientAppointment(user_id);


			var ref7 = firebase.database().ref(`users/PatientAppointments`);
			let ref8 = firebase.database().ref(`doctors/Signup`);
			//let patientId = patient_id;
			//console.log("fetchPastPatientAppointment......");

			ref7.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
		        //console.log("snapshot...", snapshot.val());
				if(snapshot.val() != null || snapshot.val() != undefined){
					let arrPatientAppointments = Object.values(snapshot.val());
					//console.log(arrPatientAppointments);
					let arrayRequestedAppointments = [];
					arrPatientAppointments.map((data,i) => {
						//console.log("data...", data);
						
						if(data.status == "Cancelled" || data.status == "Completed"){
							ref8.orderByChild("user_id").equalTo(data.doctor_id).on("value", function(doctorval){
								//console.log("doctorval", doctorval);
								if(doctorval.val() != null || doctorval.val() != undefined){
									obj = {};
									let relevantDoctor = Object.values(doctorval.val())[0];
									//console.log("relevantDoctor", relevantDoctor);
									obj.first_name = relevantDoctor.first_name;
									obj.last_name = relevantDoctor.last_name;
									obj.avatar =relevantDoctor.avatar;
									obj.specialization = relevantDoctor.specialization;
									
									obj.patient_id = user_id;
									obj.doctor_id = data.doctor_id;
									obj.type = data.type;
									obj.schedule_date = data.schedule_date;
									obj.schedule_time = data.schedule_time;
									obj.status = data.status;
									arrayRequestedAppointments.push(obj);
									//console.log("arrayPastRequestedAppointments...", arrayRequestedAppointments);
									
									dispatch({ type: FETCH_PAST_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
								}
							});  
						}
					});
				}    	       
		    }); 



		    ////fetchUpcommingPatientAppointment(user_id);


			var ref9 = firebase.database().ref(`users/PatientAppointments`);
			let ref10 = firebase.database().ref(`doctors/Signup`);
			//let patientId = patient_id;
			//console.log("fetchUpcommingPatientAppointment......");

			ref9.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
		        ////console.log("snapshot...", snapshot.val());
				if(snapshot.val() != null || snapshot.val() != undefined){
					let arrPatientAppointments = Object.values(snapshot.val());
					//console.log(arrPatientAppointments);
					let arrayRequestedAppointments = [];
					arrPatientAppointments.map((data,i) => {
						//console.log("data...", data);
						
						if(data.status == "Confirmed"){
							ref10.orderByChild("user_id").equalTo(data.doctor_id).on("value", function(doctorval){
								//console.log("doctorval", doctorval);
								if(doctorval.val() != null || doctorval.val() != undefined){
									obj = {};
									let relevantDoctor = Object.values(doctorval.val())[0];
								//	console.log("relevantDoctor", relevantDoctor);
									obj.first_name = relevantDoctor.first_name;
									obj.last_name = relevantDoctor.last_name;
									obj.avatar =relevantDoctor.avatar;
									obj.specialization = relevantDoctor.specialization;
									obj.id = data.id;
									obj.patient_id = user_id;
									obj.doctor_id = data.doctor_id;
									obj.type = data.type;
									obj.schedule_date = data.schedule_date;
									obj.schedule_time = data.schedule_time;
									obj.status = data.status;
									arrayRequestedAppointments.push(obj);
								//	console.log("arrayPastRequestedAppointments...", arrayRequestedAppointments);
									
									dispatch({ type: FETCH_UPCOMMING_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
								}
							});  
						}
					});
				}    	       
		    });


			// fetch fetch_notifications for the current patient

			     let ref11 = firebase.database().ref(`patients/Signup`);
				 let ref12 = firebase.database().ref(`users/Notifications`);
				 let ref13 = firebase.database().ref(`users/Tokens`);
			     let ref14 = firebase.database().ref(`doctors/Signup`);

	            //console.log('......', data);
				let phoneNumber = data;
			    let outputMessagesArr = [];
				ref11.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot) {
			    	if(snapshot.val() != null || snapshot.val() != undefined){
			      		let user_id = Object.values(snapshot.val())[0].user_id;
			      		ref13.orderByChild("user_id").equalTo(user_id).on("value", function(snapToken) {
			      			if(snapToken.val() != null || snapToken.val() != undefined){
			      				let currentUserExpoToken = Object.values(snapToken.val())[0].expoToken;
			      				ref12.orderByChild("to").equalTo(currentUserExpoToken).on("value", function(snapMessage) {
			      					if(snapMessage.val() != null || snapMessage.val() != undefined){
			      						let arrMessages = Object.values(snapMessage.val());

			                  arrMessages.map((item, index) => {
			                    let obj = {};
			                    
			                    obj.message = item.message;
			                    obj.time = "4:50 pm";  
			                    //" moment(item.time).format("hh:mm")

			                    let fromExpoToken = item.from;
			                  //  console.log("fromExpoToken.....", fromExpoToken);
			                    ref13.orderByChild("expoToken").equalTo(fromExpoToken).on("value", function(snapUserId) {
			                   //  	console.log("snapUserId......", snapUserId.val());
			                      if(snapUserId.val() != null || snapUserId.val() != undefined){
			                        let currentDoctorId = Object.values(snapUserId.val())[0].user_id;
			                     //   console.log("currentDoctorId", currentDoctorId);
			                        ref14.orderByChild("user_id").equalTo(currentDoctorId).on("value", function(snapName) {
			                        //	console.log("snapName....", snapName.val());
			                        	if(snapName.val() != null || snapName.val() != undefined){
			                        		obj.name = Object.values(snapName.val())[0].first_name + " " + Object.values(snapName.val())[0].last_name;
					                        outputMessagesArr.push(obj);
					                      	dispatch({ type: FETCH_NOTIFICATIONS, payload: outputMessagesArr });
			                           	}
			                        })

			                      }
			                      
			                    })      					    
			                  })

			      					}
			      				})
			      			}
			      		})
			      	}
			    });


		}
	})	


}

export const loggedMember = () => async dispatch => {
	let ref = firebase.database().ref(`patients/Signup`);
	
	let phone_number = await AsyncStorage.getItem('phone_number');
	ref.orderByChild("phone_number").equalTo(phone_number).on("value", function(snapshot) {
       console.log('MEMBER_LOGGED=--------',snapshot.key);
       dispatch({ type: MEMBER_LOGGED, payload: snapshot.val() });
   });	
}

export const Fetch_Doctors_list = (specialization) => async dispatch => {
	let ref = firebase.database().ref(`doctors/Signup`);
	let ref1 = firebase.database().ref(`ratings`);
	 	
	    await ref.orderByChild("specialization").equalTo(specialization).on("value", function(snapshot) {
      
          if(snapshot.val() != null || snapshot.val() != undefined){
            let newResultArray = [];
            arryDoctorsList = Object.values(snapshot.val());   
                                        
              arryDoctorsList.map((data, i) => {
                let obj = {};
                let mergedObj = {};
                ref1.orderByChild("user_id").equalTo(data.user_id).on("value", function(ratings) {
                  // ref1.on("value", function(doctorData) {
                
                  if(ratings.val() != null || ratings.val() != undefined){  
                    let presentdoctorrating = Object.values(ratings.val());    
                    mergedObj = Object.assign(data, presentdoctorrating[0]); 
                    obj.user_id = mergedObj.user_id,
                    obj.name = mergedObj.first_name,
	              	obj.avatar = mergedObj.avatar,
	              	obj.rating = mergedObj.overAllRatings,
	              	obj.language = ["English"],
	              	obj.location = mergedObj.res_address,
	              	obj.repliesIn = mergedObj.responseTime,
	              	obj.experience = mergedObj.experience + "+",
	              	obj.specialization = mergedObj.specialization,
	              	obj.isOnline = true				//need to be fetched

                    newResultArray.push(obj);

                    dispatch({ type: FETCH_DOCTORS_LIST, payload: newResultArray });
                       // this returns an object of objects
                  }
                });          
              });
          }   
    	}); 
}


const fetchPatientAppointment = (patient_id) => async dispatch => {
	var ref = firebase.database().ref(`users/PatientAppointments`);
	let ref2 = firebase.database().ref(`doctors/Signup`);
	let patientId = patient_id;
	console.log("fetchPatientAppointment......");

	await ref.orderByChild("patient_id").equalTo(patientId).on("value", function(snapshot) {
       // console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let arrPatientAppointments = Object.values(snapshot.val());
			let arrayRequestedAppointments = [];
			arrPatientAppointments.map((data,i) => {
				obj = {};
				if(data.status == "requested"){
					ref2.orderByChild("doctor_id").equalTo(data.doctor_id).on("value", function(doctorval){
						if(doctorval.val() != null || doctorval.val() != undefined){
							let arrPatientAppointments = Object.values(doctorval.val())[0];
							obj.first_name = arrPatientAppointments.first_name;
							obj.last_name = arrPatientAppointments.last_name;
							obj.avatar =arrPatientAppointments.avatar;
							obj.specialization = arrPatientAppointments.specialization;
							
							obj.type = data.type;
							obj.schedule_date = data.schedule_date;
							obj.schedule_time = data.schedule_time;
							arrayRequestedAppointments.push(obj);
						//	console.log("arrayRequestedAppointments...", arrayRequestedAppointments);
							
							dispatch({ type: FETCH_PATIENT_APPOINTMENT, payload: arrayRequestedAppointments });
						}
					});  
				}
			});
		}    	       
    });  
}

/*
const FetchFavouriteDoctors = () => async dispatch => {
	//let user_id =  await AsyncStorage.getItem('user_id');
	let ref = firebase.database().ref(`patients/favourite`);
	let ref1 = firebase.database().ref(`doctors/Signup`);
	let ref2 = firebase.database().ref(`patients/Signup`);
console.log("I am in FetchFavouriteDoctors");
	let phone_number = await AsyncStorage.getItem('phone_number');
	console.log("phone.....", phone_number);
	await ref2.orderByChild("phone_number").equalTo(phone_number).on("value", function(snapshot){
		console.log("snapshot...", snapshot.val());
		if(snapshot.val() != null || snapshot.val() != undefined){
			let user_id = Object.values(snapshot.val()[0].user_id);
				console.log("user_id...", user_id);

			ref.orderByChild("patient_id").equalTo(user_id).on("value", function(snapshot) {
			
			 if(snapshot.val() != null || snapshot.val() != undefined){
				let arrycurrentPatients = Object.values(snapshot.val());  
				let newResultArray = [];
				let mergedObj = {};	
				console.log("arrycurrentPatients....", arrycurrentPatients);
				arrycurrentPatients.map((list,i) => {
					if(list.favourite == true){
						let obj = {};        
				        ref1.orderByChild("doctor_id").equalTo(list.doctor_id).on("value", function(snap) {
				          if(snap.val() != null || snap.val() != undefined){  
				            let presentfavourite = Object.values(snap.val());    
				            mergedObj = Object.assign(data, presentfavourite[0]); 
				            obj.user_id = mergedObj.user_id,
				            obj.name = mergedObj.first_name,
				          	obj.avatar = mergedObj.avatar,
				          	obj.rating = mergedObj.overAllRatings,
				          	obj.language = ["English"],
				          	obj.location = mergedObj.res_address,
				          	obj.repliesIn = mergedObj.responseTime,
				          	obj.experience = mergedObj.experience + "+",
				          	obj.specialization = mergedObj.specialization,
				          	obj.isOnline = true		//need to be fetched
				            newResultArray.push(obj);
				            console.log("newResultArray....", newResultArray);
				            dispatch({ type: FETCH_FAVOURITE_DOCTORS, payload: newResultArray });
				          }
				        });
					}
				}) 
			  }
			})
		}
	})	
}

*/