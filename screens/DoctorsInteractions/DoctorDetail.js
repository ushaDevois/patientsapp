import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

import { colors, fontSizes, gradients } from '../../constants/styles'

import Button from '../../components/Button'
import Avatar from '../../components/Avatar'
import Ratings from '../../components/Ratings'

import GlobeIcon from '../../assets/images/globe_icon.png'
import LocationIcon from '../../assets/images/location.png'
import Heart from '../../assets/images/heart.png'

import BottomNavigation from '../../components/BottomNavigation'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';

class DoctorDetail extends React.Component {
  constructor(props) {
    super(props);
//    console.log("doctorDetails....", props.doctorDetails);
    this.state = {
      favourite: false
    }
    this._videocall = this._videocall.bind(this);
    this._incomingcall = this._incomingcall.bind(this);
  }

  _onHeart = () => {    
    data = {
      doctor_id: this.props.doctorDetails.user_id,
      patient_id: this.props.doctorDetails.patient_id,
      favourite: !this.state.favourite
    }
    this.setState({favourite: !this.state.favourite});
    this.props.updateFavouriteDoctor(data);
  }

  _incomingcall(user_id){
    let data = {
        type: "audio",
        doctor_id: user_id
     }
    this.props.loggedMember();
    this.props.patient_schedule_picker_passing(data);
    Actions.patient_schedule_picker();   
  }

  _videocall(user_id){
    let data = {
        type: "video",
        doctor_id: user_id
    }
    this.props.loggedMember();
    this.props.patient_schedule_picker_passing(data);
    Actions.patient_schedule_picker();
  }

  _booking(){
    Actions.lexbot();
   // Actions.chatbot();
  }

  render() {
    let avatarSource = this.props.doctorDetails && this.props.doctorDetails.avatar
    console.log("back display", this.props.doctorDetails);
    return (
      <View style={styles['container']}>
        {this.props.doctorDetails && <View style={styles.profile}>
          <View style={[styles.row]}>
            <View style={{flex: 1}}>
              <Avatar isOnline={this.props.doctorDetails.isOnline} source={avatarSource} />
            </View>
            <View style={{marginLeft: 8, marginVertical: 0, flex: 4}}>
              <Text style={{fontSize: fontSizes['md'], color: colors.dark.text, marginVertical: 4}}>{this.props.doctorDetails.name}</Text>
              <Text style={{fontSize: fontSizes['sm'], color: colors.text}}>replies in {this.props.doctorDetails.repliesIn} mins</Text>
            </View>
            <View style={{alignItems: 'center', flex: 0.8}}>
              <TouchableOpacity onPress={this._onHeart}>
                <Image source={Heart} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginVertical: 8}}>
            <Text style={{color: colors.text, fontSize: fontSizes['sm']}}>{this.props.doctorDetails.specialization}</Text>
          </View>
          <View style={{marginVertical: 8}}>
            <View style={[styles.row]}>
              <Image source={GlobeIcon} />
              <Text style={[styles.imageSideText]}>{this.props.doctorDetails.language[0]}</Text>
            </View>
            <View style={[styles.row]}>
              <Image source={LocationIcon} style={{marginLeft: 2}}/>
              <Text style={[styles.imageSideText]}>{this.props.doctorDetails.location}</Text>
            </View>
            <View style={[styles.row, {}]}>
              <Text style={styles.rating}>{this.props.doctorDetails.rating}</Text>
              <Ratings rating={this.props.doctorDetails.rating} />
            </View>
          </View>
        </View>}
        {this.props.doctorDetails && <View style={styles.contact}>
          <Text style={{fontSize: fontSizes['md'], color: colors.dark.text, marginVertical: 16}}>Consult with the doctor for 30 mins</Text>
          <View style={[styles.row, {justifyContent: 'space-between'}]}>
            <Button size="md" style={[styles.smallButton, {marginRight: 8}]} onPress={this._videocall.bind(this, this.props.doctorDetails.user_id)}>
              Video Call {"\n"}
              <Text style={{fontSize: 13, color: colors.lightWhite}}>pay $30</Text>
            </Button>
            <Button size="md" style={[styles.smallButton, {marginLeft: 8}]} onPress={this._incomingcall.bind(this, this.props.doctorDetails.user_id)}>
              Audio Call {"\n"}
              <Text style={{fontSize: 13, color: colors.lightWhite}}>pay $20</Text>
            </Button>
          </View>
          <View style={[styles.row]}>
            <Button size="lg" background="transparent" style={styles.largeButton} onPress={this._booking}>
              Chat with the doctor {"\n"}
              <Text style={{fontSize: 13, color: colors.blue}}>pay $10</Text>
            </Button>
          </View>
        </View>}
        
      </View>
    )
  }
}

function mapStateToProps({fetch_doctor_details}){
  console.log(fetch_doctor_details);
  if(fetch_doctor_details != null){
    console.log("fetch_doctor_details", fetch_doctor_details);
      return { doctorDetails: fetch_doctor_details }
  }
  return {test: "test"}
}


export default connect(mapStateToProps, actions)(DoctorDetail);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1
  },
  profile: {
    backgroundColor: colors.background,
    padding: 16,
    flex: 0.7
  },
  contact: {
    backgroundColor: colors.white,
    padding: 16,
    flex: 1
  },
  row: {
    flexDirection: 'row',
    marginVertical: 8
  },
  imageSideText: {
    padding: 2,
    alignSelf: 'center',
    marginLeft: 8,
    fontSize: fontSizes['md'],
    color: colors.dark.text
  },
  rating: {
    fontSize: fontSizes['md'],
    color: colors.text,
    alignSelf: 'center',
    padding: 2,
    marginRight: 8
  },
  smallButton: {
    flex: 1,
    paddingVertical: 8
  },
  largeButton: {
    flex: 1,
    paddingVertical: 8
  }
})

/*
<Button size="lg">
  Chat with the doctor {"\n"}
  <Text style={{fontSize: 13, }}>pay 10$</Text>
</Button>
*/
