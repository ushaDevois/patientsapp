import {  
  FETCH_FAVOURITE_DOCTORS 
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_FAVOURITE_DOCTORS:
    	return { profile_data: action.payload }
    default:
      return state;
  }
}