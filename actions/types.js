export const USER_TOKEN = 'user_token';
export const CLEAR_USER_ID = 'clear_user_id';
export const FETCH_INPUT = 'fetch_input';
export const FETCH_INPUT3 = 'fetch_input3';
export const STORE_PROFESSIONAL_DATA = 'store_professional_data';
export const FETCH_PROFILE_DATA = 'fetch_profile_data';
export const FETCH_PROFESSIONAL_DATA = 'fetch_professional_data';
export const MEMBER_LOGGEDIN = 'member_logged_in';
export const FETCH_DOCTORS_LIST = 'fetch_doctors_list';
export const FETCH_DOCTOR_DETAILS = 'fetch_doctor_details';
export const MEMBER_LOGGED = "member_logged";
export const FETCH_FAVOURITE_DOCTORS = "fetch_favourite_doctors";
export const FETCH_PATIENT_APPOINTMENT = "fetch_patient_appointment";
export const FETCH_PAST_PATIENT_APPOINTMENT = "fetch_past_patient_appointment";
export const FETCH_UPCOMMING_PATIENT_APPOINTMENT = "fetch_upcomming_patient_appointment";

export const GET_CHAT_LISTS = "get_chat_lists";
export const SUCCESS = "success";
export const FETCH_NOTIFICATIONS = "fetch_notifications";


export const FETCH_PATIENT_SCHEDULE_PICKER = "fetch_patient-schedule_picker";
export const FETCH_USERPROFILE = "fetch_userprofile";