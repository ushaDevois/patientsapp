import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView
} from 'react-native';

import { LinearGradient } from 'expo'

import moment from 'moment'
import { colors, fontSizes, gradients } from '../../constants/styles'
import TextField from '../../components/TextField'
import Button from '../../components/Button'
import Dropdown from '../../components/Dropdown'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';

class Basic extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      first_name: this.props.profile_data && this.props.profile_data.first_name,
      last_name: this.props.profile_data && this.props.profile_data.last_name,
      email: this.props.profile_data && this.props.profile_data.email,
      res_address: this.props.profile_data && this.props.profile_data.res_address,
      phone_number: this.props.phone_number && this.props.profile_data.phone_number,
      bloodGroup: this.props.bloodGroup && this.props.profile_data.bloodGroup,
      gender: this.props.gender && this.props.profile_data.gender,
      dob: this.props.dob && this.props.profile_data.dob
    }
    this._saveProfile = this._saveProfile.bind(this);
    this.handleChangeValue = this.handleChangeValue.bind(this);
  }

  

  _saveProfile(){
    let signupData = {
      first_name: this.props.profile_data.first_name || this.state.first_name,
      last_name: this.props.profile_data.last_name || this.state.last_name,
      email: this.props.profile_data.email || this.state.email,
      res_address: this.props.profile_data.res_address || this.state.res_address,
      phone_number: this.props.profile_data.phone_number || this.state.phone_number,
      bloodGroup: this.props.profile_data.bloodGroup || this.state.bloodGroup,
      gender: this.props.profile_data.gender || this.state.gender,
      dob: this.props.profile_data.dob || this.state.dob
    }
    this.props.update_profile(signupData);
  }

  handleChangeValue(field_name, textInput){
   // console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "first_name"){
      this.setState({first_name: textValue});
    }
    else if( field_value == "last_name"){
      this.setState({last_name: textValue});
    }
    else if( field_value == "email"){
      this.setState({email: textValue});
    } 
    else if( field_value == "res_address"){
      this.setState({res_address: textValue});
    }
    if( field_value == "phone_number"){
      this.setState({phone_number: textValue});
    }
    else if( field_value == "bloodGroup"){
      this.setState({bloodGroup: textValue});
    }
    else if( field_value == "gender"){
      this.setState({gender: textValue});
    } 
    else if( field_value == "dob"){
      this.setState({dob: textValue});
    }        
  }

  _uploadBtn(){
      Actions.upload_files();
  }

  _photoEdit(){
    Actions.photo_edit();
  }

  incoming_call(){
    Actions.incoming_call();
  }

  feedback(){
    Actions.feedback_form();
  }

  quoteLoader(){
    Actions.quote_loader();
  }

  randFeedback1(){
    Actions.randfb1();
  }

  randFeedback2(){
    Actions.randfb2();
  }

  randFeedback3(){
    Actions.randfb3();
  }

  render() {
    console.log("profile_data.....,,,,", this.props.profile_data);
    return (
      <ScrollView>
        <View style={styles['container']}>
        { this.props.profile_data &&
          <View style={styles['form']}>
            <View style={styles['formItemTextRow']}>
              
               <TextField
                  label="FIRST NAME"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.first_name}
                  onChange={this.handleChangeValue}
                  prop_name="first_name"
                  placeholder={this.props.profile_data.first_name}
                />

               <TextField
                  label="LAST NAME"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.last_name}
                  onChange={this.handleChangeValue}
                  prop_name="last_name"
                  placeholder={this.props.profile_data.last_name}
                />
            </View>


            <View style={styles['formItem']}>
              <TextField
                  label="PHONE"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.phone_number}
                  onChange={this.handleChangeValue}
                  prop_name="phone_number"
                  placeholder={this.props.profile_data.phone_number}
                />
            </View>
            <View style={styles['formItem']}>
              
              <TextField
                  label="EMAIL"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.email}
                  onChange={this.handleChangeValue}
                  prop_name="email"
                  placeholder={this.props.profile_data.email}
                />
            </View>
            <View style={styles['formItem']}>
              <TextField
                  label="GENDER"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.gender}
                  onChange={this.handleChangeValue}
                  prop_name="gender"
                  placeholder={this.props.profile_data.gender}
                />
            </View>
            <View style={styles['formItem']}>
              
              <TextField
                  label="DATE OF BIRTH"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.dob}
                  onChange={this.handleChangeValue}
                  prop_name="dob"
                  placeholder={this.props.profile_data.dob}
                />

            </View>
            <View style={styles['formItem']}>
              <TextField
                  label="BLOOD GROUP"
                  style={{flex: 1, marginRight: 8}}
                  inputStyle={{}}
                  value={this.props.profile_data.bloodGroup}
                  onChange={this.handleChangeValue}
                  prop_name="bloodGroup"
                  placeholder={this.props.profile_data.bloodGroup}
                />
            </View>
          </View>
        }
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._photoEdit} >Photo Crop Editing</Button>
        </LinearGradient>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.incoming_call} >Incoming Call</Button>
        </LinearGradient>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.feedback} >Call Feedback</Button>
        </LinearGradient>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.quoteLoader} >Quote Loader</Button>
        </LinearGradient>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.randFeedback1} >Random Feedback 1</Button>
        </LinearGradient>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.randFeedback2} >Random Feedback 2</Button>
        </LinearGradient>

        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this.randFeedback3} >Random Feedback 3</Button>
        </LinearGradient>
      </View>
      </ScrollView>
    )
  }
}

function mapStateToProps({fetchInput}){
  console.log("fetchInput////", fetchInput);
  if(fetchInput != null || fetchInput != undefined){
    let profile_data = Object.values(fetchInput)[0];
    return { profile_data: profile_data }
  }
  return {test: "test"}
}

export default connect(mapStateToProps, actions)(Basic);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white,
    paddingBottom: 30
  },
  title: {
    fontSize: 20,
    color: colors.dark.text,
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4,
    width: 200,
  },
  headerCard: {
    paddingHorizontal: 24,
    paddingVertical: 4
  },
  back: {
    flexDirection: 'row',
    marginBottom: 8
  },
  form: {
    paddingHorizontal: 24
  },
  bottomCard: {
    flex: 0.8,
    paddingHorizontal: 24,
    paddingVertical: 20,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  formItemTextRow:{
    marginVertical: 8,
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    flex: 1
  },
  label: {
    paddingVertical: 4,
    color: colors.text,
    fontSize: 13
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 10,
    backgroundColor: colors.fadedWhiteText
  }
})


