import {
  FETCH_NOTIFICATIONS
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const fetch_notifications = () => async dispatch => {
		
	  let ref11 = firebase.database().ref(`patients/Signup`);
         let ref12 = firebase.database().ref(`users/Notifications`);
         let ref13 = firebase.database().ref(`users/Tokens`);
           let ref14 = firebase.database().ref(`doctors/Signup`);

              //console.log('......', data);
        let phoneNumber = await AsyncStorage.getItem('phone_number');
          let outputMessagesArr = [];
        ref11.orderByChild("phone_number").equalTo(phoneNumber).on("value", function(snapshot) {
            if(snapshot.val() != null || snapshot.val() != undefined){
                let user_id = Object.values(snapshot.val())[0].user_id;
                ref13.orderByChild("user_id").equalTo(user_id).on("value", function(snapToken) {
                  if(snapToken.val() != null || snapToken.val() != undefined){
                    let currentUserExpoToken = Object.values(snapToken.val())[0].expoToken;
                    ref12.orderByChild("to").equalTo(currentUserExpoToken).on("value", function(snapMessage) {
                      if(snapMessage.val() != null || snapMessage.val() != undefined){
                        let arrMessages = Object.values(snapMessage.val());

                        arrMessages.map((item, index) => {
                          let obj = {};
                          
                          obj.message = item.message;
                          obj.time = "4:50 pm";  
                          //" moment(item.time).format("hh:mm")

                          let fromExpoToken = item.from;
                        //  console.log("fromExpoToken.....", fromExpoToken);
                          ref13.orderByChild("expoToken").equalTo(fromExpoToken).on("value", function(snapUserId) {
                         //   console.log("snapUserId......", snapUserId.val());
                            if(snapUserId.val() != null || snapUserId.val() != undefined){
                              let currentDoctorId = Object.values(snapUserId.val())[0].user_id;
                           //   console.log("currentDoctorId", currentDoctorId);
                              ref14.orderByChild("user_id").equalTo(currentDoctorId).on("value", function(snapName) {
                              //  console.log("snapName....", snapName.val());
                                if(snapName.val() != null || snapName.val() != undefined){
                                  obj.name = Object.values(snapName.val())[0].first_name + " " + Object.values(snapName.val())[0].last_name;
                                  outputMessagesArr.push(obj);
                                  dispatch({ type: FETCH_NOTIFICATIONS, payload: outputMessagesArr });
                                  }
                              })

                            }
                            
                          })                    
                        })

                      }
                    })
                  }
                })
              }
          });

}