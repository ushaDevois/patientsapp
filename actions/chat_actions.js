import firebase from 'firebase';
import addToNotifications from './notification_actions';

class Backend {
  uid = '';
  messagesRef = null;
  // initialize Firebase Backend
 
  setUid(user_id, to) {
    this.user_id = user_id;
    this.to = to;
  }
  get_user_id() {
    return this.user_id;
  }
  get_doctor_id() {
    return this.to;
  }
  // retrieve the messages from the Backend
  
loadMessages(callback) {
    this.messagesRef = firebase.database().ref('users/messages');
    this.messagesRef.off();
    let user_id = this.get_user_id();
    let to = this.get_doctor_id();
    const onReceive = (data) => {
      const message = data.val();
      if((message.to == to && message.user._id == user_id) || (message.to == user_id && message.user._id == to)){
      callback({
        _id: data.key,
        text: message.text,
        createdAt: new Date(message.createdAt),
        user: {
          _id: message.user._id,
          name: message.user.name,
        },
      });
    }
    };
    this.messagesRef.on('child_added', onReceive);
  }
  // send the message to the Backend
  sendMessage(message, id) {
    for (let i = 0; i < message.length; i++) {
      this.messagesRef.push({
        to: id,
        text: message[i].text,
        user: message[i].user,
        createdAt: firebase.database.ServerValue.TIMESTAMP,
      });

      addToNotifications.frameNotification(message[i].user._id, id, message[i].text);
    }
  }
  // close the connection to the Backend
  closeChat() {
    if (this.messagesRef) {
      this.messagesRef.off();
    }
  }
}

export default new Backend();
