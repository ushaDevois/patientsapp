import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {
  Button
} from 'react-native-elements';

import AppIntroSlider from 'react-native-app-intro-slider';
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo';

import { colors, gradients, fontSizes } from '../../constants/styles'
import * as actions from '../../actions';
import { connect } from 'react-redux';

const slides = [
  {
    key: 'somethun',
    text: 'Welcome.\nTo Patients Health App',   
  },
  {
    key: 'somethun-dos',
    text: 'Connect.\nWith the Doctor',  
  },
  {
      key: 'somethun1',
      text: 'Find New Specialist',
   }
];

class WelcomeScreen extends React.Component {
  
  _onDone = () => {
     Actions.terms();
  }
  
  componentDidMount(){
   // this.props.store_ratings();
  }

  render() {  
    return (
      <LinearGradient style={styles.container} colors={gradients.greenBlue}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
          <AppIntroSlider
            slides={slides}
            onDone={this._onDone}
          />
        </View>       
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    alignItems: 'center'
  },
  text: {
    fontSize: fontSizes['lg'],
    color: colors.white,
    textAlign: 'center'
  }
});

export default connect(null, actions)(WelcomeScreen);