import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
  } from 'react-native';
import moment from 'moment'

import { colors, fontSizes } from '../../constants/styles'
import Lightbox from './BaseLightbox'

import Button from '../../components/Button'
import Avatar from '../../components/Avatar'
import UserCallSchedule from '../../components/UserCallSchedule'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';


class RemoveFavoriteDoctor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      doctor: props.doctorDetails
    }
  }

  _cancelAppointment = () => {
    data = {
      doctor_id: this.props.doctorDetails.user_id,
      patient_id: this.props.patient_id,
      favourite: false
    }
    
    this.props.updateFavouriteDoctor(data);
    Actions.favorite_doctor();
  }

  closeModal = () => {
    this.setState({visible: false})
  }

  render() {
    return (
      <Lightbox vertical={0.6} align="bottom" visible={this.state.visible}>
        <View style={styles.container}>
          <Text style={styles.title}>Are you sure you want to remove Dr. Mamie Wagner from favourites?</Text>
          <UserCallSchedule style={styles.doctor} data={this.state.doctor} />
          <View style={styles.buttons}>
            <View style={[styles.row]}>
              <Button size="lg" background={colors.red} style={styles.largeButton} onPress={this._cancelAppointment}>
                Yes, remove from favorites
              </Button>
            </View>
            <View style={[styles.row]}>
              <Button size="lg" background="transparent" style={styles.largeButton} onPress={this.closeModal}>
                Close
              </Button>
            </View>
          </View>
        </View>
      </Lightbox>
    )
  }
}


function mapStateToProps({member_logged}){
  if(member_logged != null){
    
    let patient_id = Object.values(member_logged)[0].user_id;
      return { patient_id: patient_id }
  }
  return {test: "test"}
}


export default connect(mapStateToProps, actions)(RemoveFavoriteDoctor);


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    marginTop: 16
  },
  title: {
    fontSize: fontSizes['md'],
    color: colors.dark.text,
    marginVertical: 16
  },
  doctor: {
    marginVertical: 16
  },
  buttons: {
    marginVertical: 16
  },
  largeButton: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    marginVertical: 8
  },
})