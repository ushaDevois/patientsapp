import firebase from 'firebase';
import addToNotifications from './notification_actions';


class  updatePatientAppointment {

  randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
  }
  
  PatientAppointment (time,date,doctor_id, patient_id, type) {
      let msg;
      var randomTask_id = this.randomString(15, '');
      //console.log(typeof(date), date.toString(), "2018-08-18T05:01:32.076Z".split("-"));
      let extractDate = date.toString();
      const Data = {
        id: randomTask_id,
        patient_id: patient_id,
        doctor_id: doctor_id,
        schedule_date: extractDate,
        schedule_time: time,
        type: type,
        status: "Pending"
      }

      //console.log('patientAppointment....', Data);

      firebase.database().ref(`users/PatientAppointments`)
          .push(Data);
      
      
      if(type == 'audio'){
        msg = " you have a request from a patient for Audio appointment";
      }
      else if(type == 'video'){
        msg = " you have a request from a patient for Video appointment";
      }
      console.log('patient_id= ', patient_id);      
      console.log('doctor_id= ', doctor_id);
      
      addToNotifications.frameNotification(patient_id, doctor_id, msg);
  }
}

export default new updatePatientAppointment();
