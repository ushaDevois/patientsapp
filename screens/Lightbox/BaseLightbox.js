import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Animated, Dimensions, Button, PanResponder, TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../../constants/styles'

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

export default class BaseLightbox extends Component {
  static propTypes = {
    children: PropTypes.any,
    horizontalPercent: PropTypes.number,
    verticalPercent: PropTypes.number,
  }

  constructor(props) {
    super(props);
    
    this.state = {
      opacity: new Animated.Value(0),
      visible : this.props.visible || true,
      _panResponder: PanResponder.create({
        onStartShouldSetPanResponder: this._handleStartShouldSetPanResponder,
      }),
      currentRoute: Actions.currentScene
    };

    this._renderLightBox = this._renderLightBox.bind(this);
  }

  _handleStartShouldSetPanResponder = (e: Object, gestureState: Object): boolean => {
    return true;
  }

  componentWillReceiveProps(newProps) {
    if(newProps.visible === false) this.setState({visible: false}, () => {
      this.closeModal();
    })
  }

  componentDidMount() {
    Animated.timing(this.state.opacity, {
      duration: 100,
      toValue: 1,
    }).start();
  }
                    
  closeModal = () => {

    let route = this.state.currentRoute.slice(1, this.state.currentRoute.length);
   
    if(route == "date_picker"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.signup2());
    }
    else if(route == "patient_cancel_appointment"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.patient_bookings());
    }
    else if(route == "remove_favorite_doctor"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.favorite_doctor());
    }
    else if(route == "patient_confirm_pay"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.doctor_details());
    }
     else if(route == "patient_schedule_picker"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.doctor_details());
    }
    else if(route == "userprofile"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.chats());
    }
    else if(route == "chatupload"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.chat());
    }
   
     else if(route == "remove_card"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.patient_profile());
    }
     else if(route == "quote_loader"){
      Animated.timing(this.state.opacity, {
        duration: 100,
        toValue: 0,
      }).start(Actions.patient_profile());
    }
    
  }

  _renderLightBox() {
    const { children, align } = this.props;
    let horizontalPercent = this.props.horizontal || 1, verticalPercent = this.props.vertical || 0.8;
    if(align === 'center') {
      horizontalPercent = this.props.horizontal || 0.8;
      verticalPercent = this.props.vertical || 0.5;
    }
    const height = verticalPercent ? deviceHeight * verticalPercent : deviceHeight;
    const width = horizontalPercent ? deviceWidth * horizontalPercent : deviceWidth;

    return (
      <View {...this.state._panResponder.panHandlers}
        style={[{
          width,
          height,
          backgroundColor: 'white'
        }, this.props.style]}>
        {children}
        
      </View>
    );
  }

  render() {
    const { clickBackgroundToDismiss = true } = this.props;
    const { align } = this.props;
    let alignStyles = { justifyContent: 'center', alignItems: 'center' };
    if(align === 'bottom') alignStyles.justifyContent = 'flex-end';
    else if (align === 'top') alignStyles.justifyContent = 'flex-start';

    return (
      <TouchableWithoutFeedback disabled={!clickBackgroundToDismiss} onPress={this.closeModal}>
        <Animated.View style={[styles.container, { opacity: this.state.opacity }, alignStyles]}>
          {this._renderLightBox()}
        </Animated.View >
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
