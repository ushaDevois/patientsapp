import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList
} from 'react-native';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../../constants/styles'

import Button from '../../components/Button'
import ChatRow from '../../components/ChatRow'
import FeedbackCard from '../../components/FeedbackCard'

import BottomNavigation from '../../components/BottomNavigation'

import * as actions from '../../actions';
import { connect } from 'react-redux';


class Chats extends React.Component {
  constructor(props) {
    super(props);
    let now = moment(new Date());

    this.state = {
      chats : [
        {
          name: 'Raymond Romero',
          avatar: 'https://randomuser.me/api/portraits/men/6.jpg',
          lastMessage: {message: "Cardio science & radio labs bla bla", createdAt: moment(now).subtract(30, 'minutes')},
          unread: [{message: "bla bla 1", createdAt: new Date()}],
        },
        {
          name: 'Russell Clarke',
          avatar: 'https://randomuser.me/api/portraits/men/8.jpg',
          lastMessage: {message: "Cardio science & radio labs bla bla", createdAt: moment(now).subtract(1, 'day')},
          unread: [{message: "bla bla 1", createdAt: new Date()}, {message: "bla bla 1", createdAt: new Date()}],
        },
        {
          name: 'Lola Fox',
          avatar: 'https://randomuser.me/api/portraits/men/4.jpg',
          lastMessage: {message: "Cardio science & radio labs bla bla", createdAt: moment(now).subtract(2, 'months')},
          unread: [],
        }
      ]
    }
  }

    componentDidMount(){
      this.props.getChatLists();
    }

  _onSelect = (item) => {
    console.log(item);
    Actions.chat();
  }

  _onAvatarSelect = (item) => {
   // Actions.push("user_profile_lightbox", {data: item});
      Actions.userprofile({data: item});
  }

  _renderItem = ({item, index}) => {
    console.log("renderItem", item);
    return (
      <ChatRow data={item} onSelect={this._onSelect.bind(this, item)} onAvatarSelect={this._onAvatarSelect.bind(this, item)}/>
    )
  }

  render() {
    console.log("this.state.chatLists", this.props.chatLists, this.props.chatLists.length);
    let length = this.props.chatLists.length > 0 ? true : false;
    let chatListsArray = this.props.chatLists;
    return (
      <View style={styles['container']}>
      <Text>Testing</Text>
        {length && <View>
          <FlatList
            keyExtractor={(item, index) => 'key'+index}
            style={styles.list}
            data={chatListsArray}
            renderItem={this._renderItem}/>
        </View>}

        {length || <View>
          <Text style= {styles.noInfo}> No Chat Lists Yet </Text>
        </View>}        
      </View>
    )
  }
}

function mapStateToProps(state){
  if(state.member_logged != null){
    console.log("state.chatLists....", state.chatLists);
    console.log("state.member_logged", state.member_logged);
      let logged_value = Object.values(state.member_logged)[0];
     return {logged_value: logged_value, chatLists: state.chatLists}
  }
  
  
}

export default connect(mapStateToProps, actions)(Chats)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1
  },

  list: {
    flex: 1
  },
  noInfo: {
    color: colors.text,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
    marginLeft: 100
  }
})
