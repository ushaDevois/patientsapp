import { AsyncStorage } from 'react-native';
import firebase from 'firebase';

export const store_ratings = () => async dispatch => {
	
	let Data1 = {
		user_id: "CGw3B7vA11jBgt0",
		overAllRatings: 4,
		responseTime: 2		
	};
	let Data2 = {
		user_id: "SbemaTNsAuudMjc",
		overAllRatings: 4.2,
		responseTime: 4
	};
	let Data3 = {
		user_id: "dRi0k7AwxvHYcLo",
		overAllRatings: 3.5,
		responseTime: 3
	};
	let Data4 = {
		user_id: "riM98DhCZeLdvv7",
		overAllRatings: 3.3,
		responseTime: 1
	};
//	console.log("DATA............", Data1, Data2, Data3, Data4);
	//check including the urls
	firebase.database().ref(`ratings`).push(Data1);
	firebase.database().ref(`ratings`).push(Data2);
	firebase.database().ref(`ratings`).push(Data3);
	firebase.database().ref(`ratings`).push(Data4);
}
