import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
  } from 'react-native';
import moment from 'moment'

import { colors, fontSizes } from '../../constants/styles'
import Lightbox from './BaseLightbox'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';

import Button from '../../components/Button'
import Avatar from '../../components/Avatar'
import UserCallSchedule from '../../components/UserCallSchedule'

class PatientCancelAppointment extends React.Component {
  constructor(props) {
    super(props);
    console.log("super..", props.data);
    let dat = props.data;
    let call = props.data.type == "video" ? "Video Call" : "Audio Call";

    let formatDay = props.data.schedule_date.split(" ");
    let format = " " + formatDay[0] + " " + formatDay[1] + " " + formatDay[2] + " " + formatDay[3];
   
    this.state = {
      visible: true,
      doctor: {
        name: props.data.name,
        avatar: props.data.avatar,
        time: props.data.schedule_date,
        callType: call,
        patient_id: props.data.patient_id,
        doctor_id: props.data.doctor_id,
        id: props.data.id
      },
    }
  }

  _cancelAppointment = () => {
    this.props.cancelPatientAppoitment(this.state.doctor);
    this.setState({visible: false})
  }

  closeModal = () => {
    this.setState({visible: false})
  }

  render() {
    return (
      <Lightbox vertical={0.7} align="bottom" visible={this.state.visible}>
        <View style={styles.container}>
          <Text style={styles.title}>Are you sure you want to cancel the appointment?</Text>
          <UserCallSchedule style={styles.doctor} data={this.state.doctor} />
          <View style={styles.buttons}>
            <View style={[styles.row]}>
              <Button size="lg" background={colors.red} style={styles.largeButton} onPress={this._cancelAppointment}>
                Yes, cancel appointment
              </Button>
            </View>
            <View style={[styles.row]}>
              <Button size="lg" background="transparent" style={styles.largeButton} onPress={this.closeModal}>
                Close
              </Button>
            </View>
          </View>
        </View>
      </Lightbox>
    )
  }
}

export default connect(null, actions)(PatientCancelAppointment);


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    marginTop: 16
  },
  title: {
    fontSize: fontSizes['md'],
    color: colors.dark.text,
    marginVertical: 16
  },
  doctor: {
    marginVertical: 16
  },
  buttons: {
    marginVertical: 16
  },
  largeButton: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    marginVertical: 8
  },
})
