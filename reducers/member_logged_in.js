import {
  MEMBER_LOGGEDIN,
  MEMBER_LOGGED
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case MEMBER_LOGGEDIN:
      return action.payload
    case  MEMBER_LOGGED:
      return action.payload
    default:
      return state;
  }
}