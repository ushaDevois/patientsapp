import {
 FETCH_PATIENT_SCHEDULE_PICKER,
 FETCH_USERPROFILE
} from './types';

import { AsyncStorage } from 'react-native';
import firebase from 'firebase';


export const patient_schedule_picker_passing = (data) => async dispatch => {
  dispatch({ type: FETCH_PATIENT_SCHEDULE_PICKER, payload: data });  
}

export const fetch_userProfile = (data) => async dispatch => {
	console.log("FETCH_USERPROFILE////", data);
	dispatch({ type: FETCH_USERPROFILE, payload: data});
}
