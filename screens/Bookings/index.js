import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';

import { colors, fontSizes, gradients } from '../../constants/styles'
import { Actions } from 'react-native-router-flux';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';

import UpcomingBookings from './Upcoming'
import PastBookings from './Past'
import RequestedBookings from './Requested'

import BottomNavigation from '../../components/BottomNavigation'

import Avatar from '../../components/Avatar'
import * as actions from '../../actions';
import { connect } from 'react-redux';


const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

class PatientBookings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: {
        index: 2,
        routes: [
          { key: 'upcoming', title: 'Upcoming' },
          { key: 'past', title: 'Past' },
          { key: 'requested', title: 'Requested' },
        ],
      },

      patient: {
        name: "John Doe",
        avatar: "https://randomuser.me/api/portraits/men/10.jpg",
        age: "26 years"
      }
    }
  }

  _handleIndexChange = index =>
  this.setState({
    tabs: {...this.state.tabs, index}
  });

_renderHeader = props => (
  <TabBar
    {...props}
    renderLabel={scene => <Text style={[styles.label, {color: scene.focused ? colors.blue : colors.lightText}]}>{scene.route.title}</Text>}
    bounces={false}
    indicatorStyle={styles.indicator}
    style={styles.tabbar}
    tabStyle={styles.tab}
  />
);

_renderScene = SceneMap({
  upcoming: UpcomingBookings,
  past: PastBookings,
  requested: RequestedBookings
});

  render() {
    const { patient } = this.state;
    let item = {
      avatar: this.props.member.avatar,
      name: this.props.member.first_name + " " + this.props.member.last_name,
      age: "26 years"
    }
    console.log("MEMBER...", this.props.member);
    return (
      <View style={styles.container}>
        {this.props.member && <View style={{flexDirection: 'row', padding: 16}}>
          <Avatar source={item.avatar} />
          <View style={{marginHorizontal: 16}}>
            <Text style={{color: colors.dark.text, fontSize: fontSizes['md']}}>{item.name}</Text>
            <Text style={{color: colors.text, fontSize: fontSizes['sm'], marginVertical: 4}}>{item.age}</Text>
          </View>
        </View>}
        <TabViewAnimated
         useNativeDriver
         style={[styles.tabs]}
         navigationState={this.state.tabs}
         renderScene={this._renderScene}
         renderHeader={this._renderHeader}
         onIndexChange={this._handleIndexChange}
         initialLayout={initialLayout}/>
         
      </View>
    )
  }
}

function mapStateToProps({member_logged}){

  let member = Object.values(member_logged)[0];
 
  if(Object.keys(member_logged).length === 0 && member_logged.constructor === Object){
    return { member_logged: null }
  }

  if(member_logged != null){
    return { member: member }
  }
  
  return { test: "test" }
}

export default connect(mapStateToProps, actions)(PatientBookings);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 50/2
  },
  tabs: {

  },
  indicator: {
    backgroundColor: colors.blue,
    height: 3
  },
  tabbar: {
    backgroundColor: colors.background,
    shadowOpacity: 0,
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
    },
    elevation: 0
  },
  tab: {

  },
  label: {
    margin: 8,
    fontSize: fontSizes['sm'],
    color: colors.lightText
  }
})
