import React from 'react';
import { StyleSheet, TouchableOpacity, View, TextInput, Image } from 'react-native';

import { GiftedChat } from 'react-native-gifted-chat';
import Backend from '../../actions/chat_actions';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../../constants/styles'
import Attachment from '../../assets/images/attachment.png'
import Send from '../../assets/images/send_message.png'


class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      typingText: true    
    };
  }

  _onAttach = () => {
   // Actions.push("chat_uplaod_lightbox")
    Actions.chatupload();
  }

  _onSend = () => {

  }

  renderFooter = (props) => {
    if (this.state.typingText) {
      return (
        <View style={styles.footer}>
          <View style={styles.inputWrapper}>
            <TextInput
              style={styles.input}
              multiline
              underlineColorAndroid="transparent"
              placeholder="Type your message here"
              placeholderTextColor={colors.lightText}/>
            <TouchableOpacity style={styles.attachment} onPress={this._onAttach}>
              <Image source={Attachment} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.send} onPress={this._onSend}>
            <Image source={Send} />
          </TouchableOpacity>
        </View>
      );
    }
    return null;
  }

   renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#f0f0f0',
          }
        }}
      />
    );
  }

  renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 15,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  }

  render() {
    //let patientName = '';
    /*let arryTasks = Object.values(this.props.patient_name);
      if(arryTasks.length > 0) {
        patientName = arryTasks[0].name;
      }
     */
     let patientName = "TESTING DEVOIS";
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={(message) => {
          Backend.sendMessage(message, "104822516826198");
        }}
        user={{
          _id: "103044747005783", // this.props.userId,
          name: patientName,
        }}
        renderFooter={this.renderFooter}
      />
    );
  }
  componentDidMount() {
    //this.props.getCurrentChatMessages(this.props.userId, this.props.doctor_id);
    let userId = "103044747005783";// this.props.userId;
    let doctor_id = "104822516826198"; //this.props.doctor_id;
    Backend.setUid(userId, doctor_id);
   // this.props.getPatientName(this.props.userId);
    Backend.loadMessages((message) => {
      this.setState((previousState) => {
        return {
          messages: GiftedChat.append(previousState.messages, message),
        };
      });
    });
    
  }
  componentWillUnmount() {
    Backend.closeChat();
  }
}
/*
ChatScreen.defaultProps = {
  name: 'John Smith',
};

ChatScreen.propTypes = {
  name: React.PropTypes.string,
};
*/

function mapStateToProps(state){
 //userId: state.userId.userId
  return { patient_name: state.patient_name, getChatMessages: state.getChatMessages };
}

export default connect(mapStateToProps, actions)(Chat);


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  list: {
    paddingHorizontal: 16
  },
  message: {
    flexDirection: 'row',
    marginVertical: 4,
  },
  messageWrapper: {
    flex: 0.8,
    borderRadius: 15,
     padding: 8
  },
  messageText: {
    flexWrap: 'wrap',
    fontSize: fontSizes['sm']
  },
  messageTime: {
    alignSelf: 'flex-end',
    fontSize: fontSizes['x-sm']
  },
  footer: {
    backgroundColor: colors.white,
    padding: 8,
    flexDirection: 'row',
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: 35/2,
    marginHorizontal: 8
  },
  inputWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 8,
  },
  input: {
    flex: 1,
    borderRadius: 20,
    backgroundColor: colors.dark.gray,
    paddingHorizontal: 16
  },
  send: {
    width: 40,
    height: 40,
    borderRadius: 40/2,
    backgroundColor: colors.blue,
    alignItems: 'center',
    justifyContent: 'center'
  },
  attachment: {
    position: 'absolute',
    right: 0,
    paddingRight: 20,
    paddingTop: 10
  }
})
