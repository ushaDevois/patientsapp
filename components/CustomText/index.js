import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { colors, fontSizes } from '../../constants/styles'


export const CustomText = (props) => (
  <Text style={[styles.defaultText, props.style, props.bold && styles.bold]}>
    {props.children}
  </Text>
)


const styles = StyleSheet.create({
  defaultText: {
    fontFamily: 'circularstd-book',
    fontSize: fontSizes.text,
    color: colors.text
  }

})
