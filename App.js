import Expo, { AppLoading, Asset, Font, Notifications } from 'expo';

import React from 'react';
import { Platform, StatusBar, StyleSheet, Text, View, Alert, TextInput } from 'react-native';
import { Provider } from 'react-redux';

import store from './store';
import RouterClass from './RouterClass';
import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyB_9lf951N_nKUWpquwpYCIJXIlf7UDcUU",
  authDomain: 'doctorapp-4ddcb.firebaseapp.com', //doctor2-f874a.firebaseapp.com',
  databaseURL: 'https://doctorapp-4ddcb.firebaseio.com/',
  storageBucket: 'doctorapp-4ddcb.appspot.com'
};

firebase.initializeApp(config);

export default class App extends React.Component {

  state = {
    isLoadingComplete: false,
  };


  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/MobileRegistration.png'),
      ]),
      Font.loadAsync({
        'circularstd-book': require('./assets/fonts/circularstd-book-webfont.ttf'),
        'circularstd-medium': require('./assets/fonts/circularstd-medium-webfont.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    SetDefaultFontFamily();
    this.setState({ isLoadingComplete: true });
  };

  render() {

    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
//      console.log("true condition");
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={store}>
          <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <RouterClass />
          </View>
        </Provider>
      );
    }
}
}


SetDefaultFontFamily = () => {
    let components = [Text, TextInput]

    const customProps = {
        style: {
            fontFamily: "circularstd-book",
            fontSize: 15
        }
    }

    for(let i = 0; i < components.length; i++) {
        const TextRender = components[i].prototype.render;
        const initialDefaultProps = components[i].prototype.constructor.defaultProps;
        components[i].prototype.constructor.defaultProps = {
            ...initialDefaultProps,
            ...customProps,
        }
        components[i].prototype.render = function render() {
            let oldProps = this.props;
            this.props = { ...this.props, style: [customProps.style, this.props.style] };
            try {
                return TextRender.apply(this, arguments);
            } finally {
                this.props = oldProps;
            }
        };
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});