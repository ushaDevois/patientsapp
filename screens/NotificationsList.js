import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView
} from 'react-native';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';

import { colors, fontSizes } from '../constants/styles'

import Button from '../components/Button'
import NotificationRow from '../components/NotificationRow'
import FeedbackCard from '../components/FeedbackCard'

import BottomNavigation from '../components/BottomNavigation'

import * as actions from '../actions';
import { connect } from 'react-redux';

class NotificationsList extends React.Component {
  constructor(props) {
    super(props);
    let now = moment(new Date());

    this.state = {
      chats : [
        {
          name: 'Raymond Romero',
          lastMessage: {message: "Dr. Stephen Colins has accepted your appointent request", createdAt: moment(now).subtract(30, 'minutes')},
          unread: [],
        },
        {
          name: 'Russell Clarke',
          lastMessage: {message: "Dr. Adam Romero has changed the call timings to 3PM 2nd Jan, Saturday", createdAt: moment(now).subtract(1, 'day')},
          unread: [],
        },
        {
          name: 'Lola Fox',
          lastMessage: {message: "Video Call with Dr. Stephen Collinds will start in another 20 mins", createdAt: moment(now).subtract(2, 'months')},
          unread: [],
        }
      ]
    }
  }

  componentDidMount(){
    this.props.fetch_notifications();
  }

  _renderItem = ({item, index}) => {
    return (
      <NotificationRow data={item}/>
    )
  }

  render() {
    let now = moment(new Date());
    let notifications = this.props.notificationsArr;
    let notifications_arr = [];
    if(this.props.notificationsArr){
      notifications.map((data,i) => {
        let obj = {};
        obj.name = data.name;
        obj.lastMessage = {
          message: data.message,
          createdAt: data.time,
          unread: []
        }
        notifications_arr.push(obj);
      });
    }
    
    return (
      <ScrollView>
        {this.props.fetch_notifications && 
          <View style={styles['container']}>
            <FlatList
              keyExtractor={(item, index) => 'key'+index}
              style={styles.list}
              data={notifications_arr}
              renderItem={this._renderItem}/>
           
          </View>
        }
      </ScrollView>
    )
  }
}

function mapStateToProps({fetch_notifications}){
let notifications = fetch_notifications;
  if(fetch_notifications && fetch_notifications.length > 0){
    return { notificationsArr: notifications }
  }

  return { test: "test" }

}

export default connect(mapStateToProps, actions)(NotificationsList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1
  },

  list: {
    flex: 1
  }
})