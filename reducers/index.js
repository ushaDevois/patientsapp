import { combineReducers } from 'redux';
import userToken from './userToken_reducer';
import member_logged from './member_logged_in';
import fetchInput from './fetch_profile_data';
import fetchDoctor_data from './fetch_doctor_list';
import fetch_doctor_details from './fetch_doctor_details';
import favouriteDoctor from './favourite_doctors';
import patient_appointments from './fetch_patient_appointments';
import past_appointments from './fetch_past_patient_appointments';
import upcomming_appointments from './fetch_upcomming_patient_appointments';
import chatLists from './fetch_chatLists';
import fetch_notifications from './fetch_notifications';
import fetch_patient_schedule_picker from './fetch_patient_schedule_picker';
import fetch_userProfile from './fetch_userProfile';

export default combineReducers({
  userToken,
  member_logged,
  fetchInput,
  fetchDoctor_data,
  fetch_doctor_details,
  favouriteDoctor,
  patient_appointments,
  past_appointments,
  upcomming_appointments,
  chatLists,
  fetch_notifications,
  fetch_patient_schedule_picker,
  fetch_userProfile
});