import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
  } from 'react-native';

import { colors, fontSizes } from '../../constants/styles'
import Lightbox from './BaseLightbox'
import Avatar from '../../components/Avatar'
import Ratings from '../../components/Ratings'

import GlobeIcon from '../../assets/images/globe_icon.png'
import LocationIcon from '../../assets/images/location.png'
import Button from '../../components/Button'
import { Actions } from 'react-native-router-flux';
import * as actions from '../../actions';
import { connect } from 'react-redux';


class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    }
  }

  closeModal = ()=> {
    this.setState({visible: false})
  }

  closeLightbox = () => {
    Actions.pop();
  }

  render() {
   // const { data } = this.props;
   //
    return (
      <Lightbox align="center" visible={this.state.visible} style={styles.container}>
        {this.props.userProfile &&  <View>
          <View style={[styles.row]}>
            <Avatar source={this.props.userProfile.avatar} />
            <View style={{margin: 8}}>
              <Text style={styles.name}>{this.props.userProfile.name}</Text>
              <Text style={styles.experience}>{this.props.userProfile.experience} years of experience</Text>
            </View>
          </View>
          <Text style={styles.speciality}>{this.props.userProfile.specialization}</Text>
          <View>
            <View style={[styles.row]}>
              <Image source={GlobeIcon} />
              <Text style={[styles.imageSideText]}>{this.props.userProfile.language[0]}</Text>
            </View>
            <View style={[styles.row]}>
              <Image source={LocationIcon} style={{marginLeft: 2}}/>
              <Text style={[styles.imageSideText]}>{this.props.userProfile.location}</Text>
            </View>
          </View>
          <View style={[styles.row, {justifyContent: 'center', marginTop: 32}]}>
            <Text style={styles.rating}>{this.props.userProfile.rating}</Text>
            <Ratings rating={this.props.userProfile.rating} />
          </View>
        </View>}
        <Button size="sm" onPress={this.closeLightbox}>
          Close
        </Button>
      </Lightbox>
    )
  }
}

function mapStateToProps({fetch_userProfile}){
  if(fetch_userProfile != null){
    console.log("fetch_userProfile////", fetch_userProfile);
     return {userProfile: fetch_userProfile}
  }
  return {test: "test"}
}

export default connect(mapStateToProps, actions)(UserProfile);

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    padding: 16,
    flexDirection: 'column',
    flex: 0
  },
  row: {
    flexDirection: 'row',
    marginVertical: 8
  },
  name: {
    color: colors.dark.text,
    fontSize: fontSizes['md']
  },
  experience: {
    fontSize: fontSizes['sm'],
    color: colors.text,
    marginVertical: 4
  },
  speciality: {
    fontSize: fontSizes['sm'],
    color: colors.text,
    marginBottom: 32,
    marginTop: 16
  },
  imageSideText: {
    padding: 2,
    alignSelf: 'center',
    marginLeft: 8
  },
  rating: {
    fontSize: fontSizes['md'],
    alignSelf: 'center',
    padding: 2,
    marginRight: 8
  }
})
