import firebase from 'firebase';
import moment from 'moment';

class  addToNotifications {

  addNotification (from, to, message) {
    const Data = {
      from: from,
      to: to,
      message: message
    }

    firebase.database().ref(`users/Notifications`)
        .push(Data);
  }

  frameNotification (from, to, message) {
      let patientExpoToken;
      let doctorExpoToken;
      console.log("from and to ....", from, to);
      let ref = firebase.database().ref(`users/Tokens`);
      self = this;
      ref.orderByChild("user_id").equalTo(to).on("value", function(doctorData) {
        if(doctorData.val() != null || doctorData.val() != undefined){
            let data = Object.values(doctorData.val());
            doctorExpoToken = data[0].expoToken;
        
          ref.orderByChild("user_id").equalTo(from).on("value", function(patientData) {
            if(patientData.val() != null || patientData.val() != undefined){
                let data = Object.values(patientData.val());
                patientExpoToken = data[0].expoToken;
                let now = "4:50 pm";  //moment(new Date());
                self.addNotification(patientExpoToken, doctorExpoToken, message);
                const Data = {
                  from: patientExpoToken,
                  to: doctorExpoToken,
                  message: message,
                  time: now
                }
                console.log("....Data.......", Data);

                firebase.database().ref(`users/Notifications`)
                .push(Data);
                
            } 
          }); 
        }

      }); 
  }  
}

export default new addToNotifications();