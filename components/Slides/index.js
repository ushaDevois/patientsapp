import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

import TermsAndConditions from '../../screens/TermsAndConditions';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Slides extends Component {
  constructor(props){
    super(props)
   
    this.state = {
      renderFlag: false
    }
  }
   renderButton(index){
    console.log('index....', index);
    if(index === this.props.data.length - 1){
      this.props.onComplete
      return (
        null
      )
    }
  }

 
  renderSlides(){
    return this.props.data.map((slide, index) => {
      return (
        <View key={slide.text} style={[styles.slideStyle, {backgroundColor: slide.color}]}>
          <Text style={styles.slideText}>{slide.text}</Text>
          {this.renderButton(index)}
        </View>
      );
    });
  }

  render(){
    return (
      <ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator
        style={{flex: 1}}
      >
        {this.renderSlides()}
      </ScrollView>
    )
  }
}

const styles = {
  slideStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: SCREEN_WIDTH

  },

  slideText: {
    fontSize: 30,
    color: 'white',
    marginTop: 300
  },

  buttonStyle: {
    backgroundColor: '#0288D1'
  }

}

export default Slides;
