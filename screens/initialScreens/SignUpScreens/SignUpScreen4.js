import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import { DocumentPicker, ImagePicker } from 'expo';
import { LinearGradient } from 'expo'
import { colors, fontSizes, gradients } from '../../../constants/styles'
import LeftArrow from '../../../assets/images/left_arrow.png'

import Button from '../../../components/Button'
import ProgressWizard from '../../../components/ProgressWizard'
import ProgressCircle from '../../../components//ProgressCircle'

import { Actions } from 'react-native-router-flux';
import * as actions from '../../../actions';
import { connect } from 'react-redux';


class SignUpScreen4 extends Component {
  constructor(props) {
    super(props);
      this._onContinue = this._onContinue.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  _onContinue() {

  }

  _SelectFileUploads = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    console.log(result, result.uri);
    if(result.type == "success"){
      this.props.uploadAsFile(result.uri);
    }
  }

  _submitNext() {
      Actions.uploadcertificates();
  }

  _prevScreen(){
 //   Actions.signup3();
  }

  render() {
    return (
      <View style={styles['container']}>
        <LinearGradient colors={gradients['grayWhite']} style={styles['topCard']}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={{color: colors.text, width: 200}}>Create an account with new phone number </Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={4}/>
          </View>
        </LinearGradient>

         <TouchableOpacity style={styles.back} onPress={this._prevScreen}>
          <Image source={LeftArrow} style={{marginRight: 8}}/>
          <Text style={{color: colors.blue, fontSize: fontSizes['sm']}}>BACK</Text>
        </TouchableOpacity>


        <View style={styles['body']}>
          <Text style={{color: colors.dark.text, marginVertical: 10}}>Upload your certificates</Text>
          <Text style={{color: colors.text}}>
            Your certificates will not be shared with anyone. This is purely for our internal use to verify your identity.
          </Text>
          <Button style={{marginVertical: 20}} onPress={this._SelectFileUploads} size='lg'>Select files from phone</Button>
          
        </View>    
        
          <LinearGradient colors={gradients['whiteGray']} style={styles['bottomCard']}>
            <Button size='lg' onPress={this._submitNext} >Continue</Button>
          </LinearGradient>
       
      </View>
    )
  }
}

function mapStateToProps({fetchInput}){
  if(fetchInput != null){
    console.log("fetchInput.....", fetchInput.signup_data);
     return fetchInput
  }
  return {test: "test"}
}
export default connect(mapStateToProps, actions)(SignUpScreen4);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  topCard: {
    paddingHorizontal: 32,
    paddingTop: 16,
    flex: 1.5
  },
  body:{
    paddingTop: 40, //TODO
    flex: 4,
    paddingHorizontal: 32,
    backgroundColor: colors.white
  },
  
  bottomCard: {
    padding: 24,
    justifyContent: 'center'
  },
  title: {
    fontFamily: 'circularstd-medium',
    marginVertical: 4,
    fontSize: fontSizes['x-lg'],
    color: colors.dark.text
  },
  upload: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.separator
  },
  upload_icon: {
    alignSelf: 'center'
  },
  upload_title: {
    color: colors.dark.text,
    flex: 4,
    alignSelf: 'center',
    marginLeft: 10
  },
   back: {
    flexDirection: 'row',
    marginTop: 45,
    marginLeft: 30
  }
})