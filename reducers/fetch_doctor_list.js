import {  
  FETCH_DOCTORS_LIST 
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_DOCTORS_LIST:
    
    	return { doctors_list: action.payload }
    default:
      return state;
  }
}