import firebase from 'firebase';
import {
	GET_CHAT_LISTS,
} from './types';
import { AsyncStorage } from 'react-native';
import _ from 'lodash';


export const getChatLists = () => async dispatch => {

    console.log("I AM IN GET CHAT LISTS");
     
    let userId = await AsyncStorage.getItem('user_id');;

  	let resultArrReceivers = [];
    let arrChatUsers = [];
    let ref = firebase.database().ref(`users/messages`);
    let ref1 = firebase.database().ref(`doctors/Signup`);
    ref.orderByChild(`user/_id`).equalTo(userId).on("value", function(snapshot) {
      console.log("USERS1...", snapshot.val());
      if(snapshot.val() != null || snapshot.val() != undefined){    
          let arryMsgUser = Object.values(snapshot.val());
          console.log("arryMsgUser", arryMsgUser);
          
          
          arryMsgUser.map((item,i) => {
            let obj = {};
            let innerobj = {};
            innerobj.message = item.text;
            innerobj.createdAt = item.createdAt;
            obj.unread = [];                        // need to get it dynamically
            obj.lastMessage = innerobj;       
            obj.to = item.to;
            ref1.orderByChild('user_id').equalTo(item.to).on("value", function(snapshot) {
              if(snapshot.val() != null || snapshot.val() != undefined){
                  let user = Object.values(snapshot.val())[0];  
                  obj.name = user.first_name + " " + user.last_name;
                  obj.avatar = user.avatar;
                  obj.user_id = user.user_id;
              }
            });
            arrChatUsers.push(obj);
          });

          console.log("arrChatUsers.....", arrChatUsers);
          let uniqueChatReceivers = arrChatUsers.map(current => current.to)
          .filter((value, index, self) => self.indexOf(value) === index)

          let uniqueobjects = getUniqueValuesOfKey(arrChatUsers, 'to');

          let chatLists = [];
          uniqueobjects.map((item, i) => {
            console.log("item...", item);
            let object = arrChatUsers.find(function (obj) { return obj.to === item; });
            console.log("object", object);
            chatLists.push(object);
          })
          
          dispatch({type: GET_CHAT_LISTS, payload: chatLists});
		  }		
	}); 
  
}

function getUniqueValuesOfKey(array, key){
  return array.reduce(function(carry, item){
    if(item[key] && !~carry.indexOf(item[key])) carry.push(item[key]);
    return carry;
  }, []);
}